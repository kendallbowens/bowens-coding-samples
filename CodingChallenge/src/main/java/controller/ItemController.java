package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.Item;
import model.ItemTypeEnums.ItemType;
import service.ItemServiceImpl;

public class ItemController {
	
	public static void viewAllItems(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		int itemListId = Integer.parseInt(req.getParameter("listId"));
		
		List<Item> allItems = new ItemServiceImpl().getAllItems(0, itemListId, false);
		
		PrintWriter printer = resp.getWriter();
		System.out.println(allItems.toString());
		printer.write(new ObjectMapper().writeValueAsString(allItems));
	}
	
	public static void viewItemById(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		int itemListId = Integer.parseInt(req.getParameter("listId"));
		
		Item item = new ItemServiceImpl().getAllItems(0, itemListId, false).get(0);
		
		PrintWriter printer = resp.getWriter();
		System.out.println(item.toString());
		printer.write(new ObjectMapper().writeValueAsString(item));
		
//		String myPath = "/resources/html/update-ITEMS.html?listId="
//				+ itemListId + "&itemId=" + item.getItemId();
//		req.getRequestDispatcher(myPath).forward(req, resp);
	}
	
	public static void addItems(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		String itemName = req.getParameter("name");
		Double itemCost = Double.parseDouble(req.getParameter("cost"));
		ItemType itemType = ItemType.valueOf(Integer.parseInt(req.getParameter("itemType")));
		int itemListId = Integer.parseInt(req.getParameter("itemList"));
		
		Item item = new Item(itemName, itemCost, itemListId, itemType);
		
		ItemServiceImpl itemServ = new ItemServiceImpl();
		
		int newItemId = itemServ.createItem(item);
		
		System.out.println(newItemId);
		
		//TODO: redirect back to prev page
		String myPath = "/CodingChallenge/resources/html/ITEMS.html?listId=" + itemListId;
		resp.sendRedirect(myPath);
		
	}
	
	public static void updateItem(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		int itemId = Integer.parseInt(req.getParameter("itemId"));
		String itemName = req.getParameter("name");
		Double itemCost = Double.parseDouble(req.getParameter("cost"));
		ItemType itemType = ItemType.valueOf(Integer.parseInt(req.getParameter("itemType")));
		int itemListId = Integer.parseInt(req.getParameter("itemList"));
		
		
		ItemServiceImpl itemServ = new ItemServiceImpl();
		
		Item updatedItem = new Item(itemId, itemName, itemCost, itemListId, itemType);
		System.out.println(updatedItem);
		
		updatedItem = itemServ.updateItem(updatedItem);
		
		String myPath = "/CodingChallenge/resources/html/ITEMS.html?listId=" + itemListId;
		resp.sendRedirect(myPath);
		
	
	}
	
	public static void deleteItem(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		System.out.println("in delete method");
		
		System.out.println("itemId");
		int itemId = Integer.parseInt(req.getParameter("itemId"));
		int itemListId = Integer.parseInt(req.getParameter("listId"));
		
		ItemServiceImpl itemServ = new ItemServiceImpl();
		
		boolean didDelete = itemServ.deleteItem(itemId);
		
		System.out.println(didDelete);
		
		String myPath = "/CodingChallenge/resources/html/ITEMS.html?listId=" + itemListId;
		resp.sendRedirect(myPath);
	}

}
