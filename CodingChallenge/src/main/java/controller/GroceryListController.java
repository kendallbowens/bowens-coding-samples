package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.GroceryList;
import service.GroceryListServiceImpl;

public class GroceryListController {
	
	public static void viewAllLists(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		List<GroceryList> allLists = new GroceryListServiceImpl().getAllLists(0);
		
		PrintWriter printer = resp.getWriter();
		System.out.println(allLists.toString());
		printer.write(new ObjectMapper().writeValueAsString(allLists));
	}
	
	public static void getListById(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		int listId = Integer.parseInt(req.getParameter("listId"));
		
//		List<GroceryList> allLists = new GroceryListServiceImpl().getAllLists(listId);
		GroceryList singleList = new GroceryListServiceImpl().getAllLists(listId).get(0);
		
		PrintWriter printer = resp.getWriter();
		System.out.println(singleList.toString());
		printer.write(new ObjectMapper().writeValueAsString(singleList));
	}
	
	
	public static void createNewList(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		System.out.println("Creating new list");
		
		String listName = req.getParameter("groceryList");
		
		GroceryList groceryList = new GroceryList(0, listName);
		
		System.out.println(groceryList.toString());
		
		GroceryListServiceImpl groceryServ = new GroceryListServiceImpl();
		
		int listId = groceryServ.createList(groceryList);
		
		String myPath = "/CodingChallenge/index.html";
		resp.sendRedirect(myPath);
		
	}
	
	public static void updateList(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		int listId = Integer.parseInt(req.getParameter("listId"));
		String listName = req.getParameter("groceryList");
		
		GroceryListServiceImpl groceryServ = new GroceryListServiceImpl();
		
		GroceryList updateList = new GroceryList(listId, listName);
		System.out.println(updateList);
		
		groceryServ.updateList(updateList);
		
		String myPath = "/CodingChallenge/index.html";
		resp.sendRedirect(myPath);
	}
	
	public static void deleteList(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
		int listId = Integer.parseInt(req.getParameter("listId"));
		
		GroceryListServiceImpl groceryServ = new GroceryListServiceImpl();
		
		boolean didDelete = groceryServ.deleteList(listId);
		
		System.out.println(didDelete);
		
		String myPath = "/CodingChallenge/";
		resp.sendRedirect(myPath);
	}
	

}
