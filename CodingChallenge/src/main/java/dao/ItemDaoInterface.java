package dao;

import java.util.List;

import model.Item;

public interface ItemDaoInterface {
	
	public int createItem (Item item);
	public Item getItem (int itemId);
	public Item updateItem (Item item);
	public boolean deleteItem (int itemId);
	List<Item> getAllItems(int itemId, int listId, boolean returnAll);

}
