package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.GroceryList;

public class GroceryListDaoImpl implements GroceryListDaoInterface {
	
	CustomConnectionFactory myConn = new CustomConnectionFactory();

	@Override
	public int createList(GroceryList list) {
		
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "INSERT INTO grocery_list (list_name) " 
			+ "VALUES (?); ";

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, list.getListName());

			ps.executeUpdate(); // <--update not query

			int insertedPK = -1; // this is a holder variable
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				insertedPK = rs.getInt("list_id");
			}

			myConn.loggy.info("A new list was created.");
			return insertedPK;

		} catch (SQLException e) {
			e.printStackTrace();
			myConn.loggy.error("A SQL Exception was thrown.");
			return -1;
		}
	}

	@Override
	public GroceryList getList(int listId) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "SELECT * FROM grocery_list " + "WHERE list_id = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, listId);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				GroceryList newList = new GroceryList(rs.getInt("list_id")
						, rs.getString("list_name"));

				return newList;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<GroceryList> getAllLists(int listId) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {
			
			String sql = "SELECT * FROM grocery_list ";

			if (listId != 0) {
				sql = sql + "WHERE list_id = ? ";
			}

			sql = sql + " ORDER BY list_id; ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			if (listId != 0) ps.setInt(1, listId);

			ResultSet rs = ps.executeQuery();

			List<GroceryList> listOfLists = new ArrayList<>();

			while (rs.next()) {
				GroceryList groceryList = new GroceryList(rs.getInt("list_id"), rs.getString("list_name"));

				listOfLists.add(groceryList);
			}
			myConn.loggy.info("All grocery lists are being viewed.");
			return listOfLists;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown.");
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public GroceryList updateList(GroceryList list) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "UPDATE grocery_list " + "SET list_name = ? "
						+ "WHERE list_id = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, list.getListName());
			ps.setInt(2, list.getListId());

			ps.executeUpdate(); // <--update not query

			myConn.loggy.info("A list was updated.");

			return this.getList(list.getListId());

		} catch (SQLException e) {
			e.printStackTrace();
			myConn.loggy.error("A SQL Exception was thrown.");
			return null;
		}
	}

	@Override
	public boolean deleteList(int listId) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "DELETE FROM grocery_item WHERE item_list = ?; ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, listId);

			ps.executeUpdate(); // <--update not query
			myConn.loggy.info("User was removed.");
			
			sql = "DELETE FROM grocery_list WHERE list_id = ?; ";

			ps = conn.prepareStatement(sql);
			ps.setInt(1, listId);

			ps.executeUpdate(); // <--update not query
			myConn.loggy.info("User was removed.");
			
			return true;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown");
			e.printStackTrace();
		}
		return false;
	}

}
