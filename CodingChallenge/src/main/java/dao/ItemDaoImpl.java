package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.GroceryList;
import model.Item;
import model.ItemTypeEnums.ItemType;

public class ItemDaoImpl implements ItemDaoInterface {

	CustomConnectionFactory myConn = new CustomConnectionFactory();
	
	@Override
	public int createItem(Item item) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "INSERT INTO grocery_item (item_name, item_cost, item_type, item_list) " 
			+ "VALUES (?, ?, ?, ?); ";

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, item.getItemName());
			ps.setDouble(2, item.getItemCost());
			ps.setInt(3, item.getItemType().getValue());
			ps.setInt(4, item.getItemList());

			ps.executeUpdate(); // <--update not query

			int insertedPK = -1; // this is a holder variable
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				insertedPK = rs.getInt("item_id");
			}

			myConn.loggy.info("A new item was created.");
			return insertedPK;

		} catch (SQLException e) {
			e.printStackTrace();
			myConn.loggy.error("A SQL Exception was thrown.");
			return -1;
		}
	}

	@Override
	public Item getItem(int itemId) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "SELECT * FROM grocery_item " + "WHERE item_id = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, itemId);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				Item newItem = new Item(rs.getInt("item_id")
						, rs.getString("item_name"), rs.getDouble("item_cost")
						, rs.getInt("item_list"), ItemType.valueOf(rs.getInt("item_type")));

				return newItem;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Item> getAllItems(int itemId, int listId, boolean returnAll) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			
			String sql = "SELECT * FROM grocery_item ";

			if (itemId != 0) {
				sql = sql + "WHERE item_id = ? ";
			}
			else if(listId != 0) {
				sql = sql + "WHERE item_list = ? ";
			}

			sql = sql + " ORDER BY item_id; ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			if (itemId != 0){
				ps.setInt(1, itemId);
			}
			else if (listId != 0) {
				ps.setInt(1, listId);
			}
			

			ResultSet rs = ps.executeQuery();

			List<Item> listOfItems = new ArrayList<>();

			// loop thru & add reimbursement to list
			while (rs.next()) {
				Item item = new Item(rs.getInt("item_id"), rs.getString("item_name")
						, rs.getDouble("item_cost"), rs.getInt("item_list"), ItemType.valueOf(rs.getInt("item_type")) );

				listOfItems.add(item);
			}
			myConn.loggy.info("All grocery lists are being viewed.");
			return listOfItems;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown.");
			e.printStackTrace();
		}

		return null;
	}


	@Override
	public Item updateItem(Item item) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "UPDATE grocery_item " + "SET item_name = ?, item_cost = ?, item_type = ?, item_list = ? "
						+ "WHERE item_id = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, item.getItemName());
			ps.setDouble (2, item.getItemCost());
			ps.setInt(3, item.getItemType().getValue());
			ps.setInt(4,  item.getItemList());
			ps.setInt(5, item.getItemId());
			

			System.out.println(ps.toString());
			ps.executeUpdate(); // <--update not query
			
			myConn.loggy.info("An item was updated.");

			return this.getItem(item.getItemId());

		} catch (SQLException e) {
			e.printStackTrace();
			myConn.loggy.error("A SQL Exception was thrown.");
			return null;
		}
	}

	@Override
	public boolean deleteItem(int itemId) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "DELETE FROM grocery_item WHERE item_id = ?; ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, itemId);

			ps.executeUpdate(); // <--update not query
			myConn.loggy.info("User was removed.");
			return true;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown");
			e.printStackTrace();
		}
		return false;
	}

}
