package service;

import java.util.List;

import model.Item;

public interface ItemServiceInterface {
	
	public int createItem (Item item);
	public Item getItem (int itemId);
	public Item updateItem (Item item);
	public boolean deleteItem (int itemId);
	List<Item> getAllItems(int itemId, int listId, boolean returnAll);

}
