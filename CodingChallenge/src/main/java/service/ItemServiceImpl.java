package service;

import java.util.List;

import dao.ItemDaoImpl;
import dao.ItemDaoInterface;
import model.Item;

public class ItemServiceImpl implements ItemServiceInterface {

	ItemDaoInterface myDao = new ItemDaoImpl();
	
	@Override
	public int createItem(Item item) {
		return myDao.createItem(item);
	}

	@Override
	public Item getItem(int itemId) {
		return myDao.getItem(itemId);
	}

	@Override
	public Item updateItem(Item item) {
		return myDao.updateItem(item);
	}

	@Override
	public boolean deleteItem(int itemId) {
		return myDao.deleteItem(itemId);
	}

	@Override
	public List<Item> getAllItems(int itemId, int listId, boolean returnAll) {
		return myDao.getAllItems(itemId, listId, returnAll);
	}

}
