package service;

import java.util.List;

import model.GroceryList;
import model.Item;

public interface GroceryListServiceInterface {
	
	public int createList (GroceryList list);
	public GroceryList getList (int listId);
	public GroceryList updateList (GroceryList list);
	public boolean deleteList (int listId);
	public List<GroceryList> getAllLists(int listId);
}
