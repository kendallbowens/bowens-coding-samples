package service;

import java.util.List;

import dao.GroceryListDaoImpl;
import dao.GroceryListDaoInterface;
import model.GroceryList;

public class GroceryListServiceImpl implements GroceryListServiceInterface {

	GroceryListDaoInterface myDao = new GroceryListDaoImpl();
	
	@Override
	public int createList(GroceryList list) {
		return myDao.createList(list);
	}

	@Override
	public GroceryList getList(int listId) {
		return myDao.getList(listId);
	}

	@Override
	public GroceryList updateList(GroceryList list) {
		return myDao.updateList(list);
	}

	@Override
	public boolean deleteList(int listId) {
		return myDao.deleteList(listId);
	}

	@Override
	public List<GroceryList> getAllLists(int listId) {
		return myDao.getAllLists(listId);
	}

}
