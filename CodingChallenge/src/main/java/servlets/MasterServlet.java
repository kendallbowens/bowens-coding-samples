package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name= "MasterServlet", urlPatterns= {"/master/*", "/grocerylist/*", "/groceryitem/*", "/json/*"})
public class MasterServlet extends HttpServlet{
	

		protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			System.out.println("MASTER SERVLET: In the doGet method!!!");
			
			AccountDispatcher.myVirtualRouter(req, resp); //HERE I am offloading my work to another entity
		}
		
		protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			System.out.println("MASTER SERVLET: In the doPost method!!!");
			
			AccountDispatcher.myVirtualRouter(req, resp);
		}
		
		protected void doPut(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
				System.out.println("MASTER SERVLET: In the doPut method!!!");
				
				AccountDispatcher.myVirtualRouter(req, resp);
			}
		
		protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
				System.out.println("MASTER SERVLET: In the doDelete method!!!");
				
				AccountDispatcher.myVirtualRouter(req, resp);
			}

}
