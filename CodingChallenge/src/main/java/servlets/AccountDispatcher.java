package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.GroceryListController;
import controller.ItemController;
import model.Item;

public class AccountDispatcher {
			
	public static void myVirtualRouter(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		System.out.println(req.getRequestURI());
		switch(req.getRequestURI()) {
		
		case "/CodingChallenge/json/viewAllLists":
			GroceryListController.viewAllLists(req, resp);
			break;
			
		case "/CodingChallenge/json/viewAllItems":
			ItemController.viewAllItems(req, resp);
			break;	
			
		case "/CodingChallenge/json/viewById":
			GroceryListController.getListById(req, resp);
			break;
			
		case "/CodingChallenge/grocerylist":
			GroceryListController.createNewList(req, resp);
			break;
			
		case "/CodingChallenge/grocerylist/update":
			GroceryListController.updateList(req, resp);
			break;
		
		case "/CodingChallenge/grocerylist/additems":
			ItemController.addItems(req, resp);
			break;
		
		case "/CodingChallenge/json/viewByItemId":
			ItemController.viewItemById(req, resp);
			break;
		
		case "/CodingChallenge/groceryitem/update":
			ItemController.updateItem(req, resp);
			break;
		
		case "/CodingChallenge/groceryitem/delete":
			ItemController.deleteItem(req, resp);
			break;
			
		case "/CodingChallenge/grocerylist/delete":
			GroceryListController.deleteList(req, resp);
			break;
		
			
		default:
			System.out.println("Bad URI.");
			break;

		}
	}

}
