package model;

import model.ItemTypeEnums.ItemType;

public class Item {
	
	private int itemId;
	private String itemName;
	private double itemCost;
	private int itemList;
	private ItemType itemType;
	
	public Item() {
		// TODO Auto-generated constructor stub
	}
	
	public Item(String itemName, double itemCost, int itemList, ItemType itemType) {
		super();
		this.itemName = itemName;
		this.itemCost = itemCost;
		this.itemList = itemList;
		this.itemType = itemType;
	}

	public Item(int itemId, String itemName, double itemCost, int itemList, ItemType itemType) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemCost = itemCost;
		this.itemList = itemList;
		this.itemType = itemType;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getItemCost() {
		return itemCost;
	}

	public void setItemCost(double itemCost) {
		this.itemCost = itemCost;
	}

	public int getItemList() {
		return itemList;
	}

	public void setItemList(int itemList) {
		this.itemList = itemList;
	}

	public ItemType getItemType() {
		return itemType;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	@Override
	public String toString() {
		return "\n\t\tItem [itemId=" + itemId + ", itemName=" + itemName + ", itemCost=" + itemCost + ", itemList=" + itemList
				+ ", itemType=" + itemType + "]";
	}
	
	
	
	
	
}