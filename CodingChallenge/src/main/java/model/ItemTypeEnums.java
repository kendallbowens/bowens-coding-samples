package model;

import java.util.HashMap;
import java.util.Map;


public class ItemTypeEnums {
	
	public enum ItemType {
		Food(1),
		Electronics(2),
		Clothing(3),
		Home(4),
		Bathroom(5),
		Other(6);
		
		
		
		private int value;
		private static Map map = new HashMap<>();
		
		private ItemType(int value) {
			this.value = value;
		}
		
		
		static {
			for (ItemType itemType : ItemType.values()) {
				map.put(itemType.value, itemType);
			}
		}
		
		public static ItemType valueOf(int itemType) {
			return (ItemType) map.get(itemType);
		}
		
		public int getValue() {
			return value;
		}
	}

}
