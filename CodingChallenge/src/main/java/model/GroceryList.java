package model;

public class GroceryList {
	
	private int listId;
	private String listName;
	
	public GroceryList() {
		// TODO Auto-generated constructor stub
	}
	
	public GroceryList(String listName) {
		super();
		this.listName = listName;
	}

	public GroceryList(int listId, String listName) {
		super();
		this.listId = listId;
		this.listName = listName;
	}

	public int getListId() {
		return listId;
	}

	public void setListId(int listId) {
		this.listId = listId;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	@Override
	public String toString() {
		return "\n\t\tList [listId=" + listId + ", listName=" + listName + "]";
	}
	
	
	
	

}
