let listId = getQueryStringValue('listId'); //replace with passed in id

document.getElementById('itemList').value = listId;

//found function to parse the query string parameter passed in
function getQueryStringValue (key) {  
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
  }
  
  