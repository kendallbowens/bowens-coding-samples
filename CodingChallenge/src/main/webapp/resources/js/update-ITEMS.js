console.log("in js file");


let URL = 'http://localhost:9001/CodingChallenge/json/viewByItemId';

let listId = getQueryStringValue('listId'); //replace with passed in id
URL = URL + '?listId=' + listId;

let itemId = getQueryStringValue('itemId'); //replace with passed in id
URL = URL + "&itemId=" + itemId;

//found function to parse the query string parameter passed in
function getQueryStringValue (key) {  
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
  }  

window.onload = function(){

	ajaxGetList();

}

function ajaxGetList(){

	console.log("starting the ajax request");
	
	fetch(URL)
		.then(function(response){
					console.log("in first response");
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					listDOMManipulation(secondResponse);
				})
}

   

function listDOMManipulation(itemJSON){

console.log("inserting values");

    //set values of the form elements
    document.getElementById('name').value = itemJSON.itemName;
    document.getElementById('cost').value = itemJSON.itemCost;
    document.getElementById('itemList').value = itemJSON.itemList;
    document.getElementById('itemId').value = itemJSON.itemId;
    
    switch (itemJSON.itemType) {
        case "Food":
            document.getElementById('food').selected = true;
            break;
        case "Electronics":
            document.getElementById('electronics').selected = true;
            break;
        case "Clothing":
            document.getElementById('clothing').selected = true;
            break;
         case "Home":
            document.getElementById('home').selected = true;
            break;
         case "Bathroom":
            document.getElementById('bathroom').selected = true;
            break;
         case "Other":
            document.getElementById('other').selected = true;
            break;
         
        default:
            break;
    }
        

}