console.log("in js file");

let URL = 'http://localhost:9001/CodingChallenge/json/viewAllItems';
let listId = getQueryStringValue('listId'); //replace with passed in id
URL = URL + '?listId=' + listId;


document.getElementById('addItemButton').href = 
	document.getElementById('addItemButton').href + "?listId=" + listId;

//found function to parse the query string parameter passed in
function getQueryStringValue (key) {  
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
  } 

let formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD'
 });

window.onload = function(){
    console.log(URL);
	ajaxViewAllItems();
}

function ajaxViewAllItems(){

	console.log("starting the ajax request");
	
	
	fetch(URL)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					itemDOMManipulation(secondResponse);
				})
}

function clickerFunction(){
    document.getElementById("filterItemStatus").addEventListener("onchange", getStatus)
}

function getStatus(){

    let url = 'http://localhost:8080/Project_1/json/viewByStatus?filterItemStatus='
        + document.getElementById("filterItemStatus").value;

    console.log(url);
    
    fetch(url)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
                    console.log("clicked");
					console.log(secondResponse);
					deleteTable(secondResponse);
                    console.log("after delete:" + document.querySelector("#itemTableBody").childElementCount);
                    itemDOMManipulation(secondResponse);
                    console.log("after repopulate:" + document.querySelector("#itemTableBody").childElementCount);
				})


}

function deleteTable(statJSON){
	let rowCount = document.querySelector("#itemTableBody").childElementCount
    console.log(document.querySelector("#itemTableBody").childElementCount);

    for (let i= 0; i < rowCount ; i++) {
        document.querySelector("#itemTableBody").deleteRow(0);
	}
}




function itemDOMManipulation(itemJSON){

	let itemURL = 'http://localhost:9001/CodingChallenge/resources/html/update-ITEMS.html';
	itemURL = itemURL + '?listId=' + listId;
		
	for(let i=0; i < itemJSON.length ; i++){
		
		let newTR = document.createElement("tr");
		let newTH = document.createElement("th");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		let newTD4 = document.createElement("td");
		let newTD5 = document.createElement("td");
		let newTD6 = document.createElement("td");
		
		//UPDATE button 
        let newLink = document.createElement('a');
        let linkButton = document.createElement("button");
        linkButton.innerHTML = "Update";
        linkButton.className = 'updateButton';
        newLink.appendChild(linkButton);
        newLink.title = 'Update';
        newLink.href = itemURL +"&itemId=" + itemJSON[i].itemId
        
        let myItemD5 = newLink;
        
        //DELETE button 
        let newDelete = document.createElement('a');
        let deleteButton = document.createElement("button");
        deleteButton.innerHTML = "DELETE";
        deleteButton.className = 'deleteButton';
        newDelete.appendChild(deleteButton);
        newDelete.title = 'DELETE';
        newDelete.href = 'http://localhost:9001/CodingChallenge/groceryitem/delete?listId=' + itemJSON[i].itemList + '&itemId=' + itemJSON[i].itemId;
        
        let myItemD6 = newDelete
		
		newTH.setAttribute("scope", "row");
		let myTextH = document.createTextNode(itemJSON[i].itemId);
		let myTextD1 = document.createTextNode(itemJSON[i].itemName);
        let myTextD2 = document.createTextNode(formatter.format(itemJSON[i].itemCost));
        let myTextD3 = document.createTextNode(itemJSON[i].itemType);
        let myTextD4 = document.createTextNode(itemJSON[i].itemList);
       

        newTH.appendChild(myTextH);
        newTD1.appendChild(myTextD1);
        newTD2.appendChild(myTextD2);
        newTD3.appendChild(myTextD3);
        newTD4.appendChild(myTextD4);
        newTD5.appendChild(myItemD5);
        newTD6.appendChild(myItemD6);
       
        
		
        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(newTD3);
        newTR.appendChild(newTD4);
        newTR.appendChild(newTD5);
        newTR.appendChild(newTD6);
        

        let newSelection = document.querySelector("#myItemTable").querySelector("#itemTableBody");
        newSelection.appendChild(newTR);
		
	}
	
}


