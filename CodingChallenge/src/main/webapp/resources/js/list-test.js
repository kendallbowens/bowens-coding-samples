
console.log("in js file");

let URL = 'http://localhost:9001/CodingChallenge/json/viewAllLists'

window.onload = function(){
    console.log(URL);
	ajaxViewAllLists();
}

function ajaxViewAllLists(){
	
	console.log("starting the ajax request");
	
	
	fetch(URL)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					listDOMManipulation(secondResponse);
				})
}

function clickerFunction(){
    document.getElementById("filterlistStatus").addEventListener("onchange", getStatus)
}

function getStatus(){

    let url = 'http://localhost:8080/Project_1/json/viewByStatus?filterlistStatus='
        + document.getElementById("filterlistStatus").value;

    console.log(url);
    
    fetch(url)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
                    console.log("clicked");
					console.log(secondResponse);
					deleteTable(secondResponse);
                    console.log("after delete:" + document.querySelector("#listTableBody").childElementCount);
                    listDOMManipulation(secondResponse);
                    console.log("after repopulate:" + document.querySelector("#listTableBody").childElementCount);
				})


}

function deleteTable(statJSON){
	let rowCount = document.querySelector("#listTableBody").childElementCount
    console.log(document.querySelector("#listTableBody").childElementCount);

    for (let i= 0; i < rowCount ; i++) {
        document.querySelector("#listTableBody").deleteRow(0);
	}
}


function listDOMManipulation(listJSON){
	
	for(let i=0; i < listJSON.length ; i++){
		
		let newTR = document.createElement("tr");
		let newTH = document.createElement("th");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		let newTD4 = document.createElement("td");
	
		
		
		newTH.setAttribute("scope", "row");
		let myTextH = document.createTextNode(listJSON[i].listId);
        let myTextD1 = document.createTextNode(listJSON[i].listName)


        //UPDATE button 
        let newLink = document.createElement('a');
        let linkButton = document.createElement("button");
        linkButton.innerHTML = "Update";
        linkButton.className = 'updateButton';
        newLink.appendChild(linkButton);
        newLink.title = 'Update';
        newLink.href = 'http://localhost:9001/CodingChallenge/resources/html/update-list.html?listId=' + listJSON[i].listId
        
        //VIEW LIST ITEMS BUTTON
        let viewLink = document.createElement('a');
        let viewLinkButton = document.createElement("button");
        viewLinkButton.innerHTML = "View Items";
        viewLinkButton.className = 'viewButton';
        viewLink.appendChild(viewLinkButton);
        viewLink.title = 'View Items';
        viewLink.href = 'http://localhost:9001/CodingChallenge/resources/html/ITEMS.html?listId=' + listJSON[i].listId

		//DELETE button 
        let newDelete = document.createElement('a');
        let deleteButton = document.createElement("button");
        deleteButton.innerHTML = "DELETE";
        deleteButton.className = 'deleteButton';
        newDelete.appendChild(deleteButton);
        newDelete.title = 'DELETE';
        newDelete.href = 'http://localhost:9001/CodingChallenge/grocerylist/delete?listId=' + listJSON[i].listId;

        let myItemD2 = newLink;
        let myItemD3 = viewLink;
        let myItemD4 = newDelete;

        newTH.appendChild(myTextH);
        newTD1.appendChild(myTextD1);
        newTD2.appendChild(myItemD2);
        newTD3.appendChild(myItemD3);
        newTD4.appendChild(myItemD4);
       
        
		
        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(myItemD2);
        newTR.appendChild(myItemD3);
        newTR.appendChild(myItemD4);
       

        let newSelection = document.querySelector("#myListTable").querySelector("#listTableBody");
        newSelection.appendChild(newTR);
		
	}
	
}