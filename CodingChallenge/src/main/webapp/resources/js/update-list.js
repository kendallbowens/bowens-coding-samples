console.log("in js file");


let URL = 'http://localhost:9001/CodingChallenge/json/viewById';

let listId = getQueryStringValue('listId'); //replace with passed in id
URL = URL + '?listId=' + listId;

//found function to parse the query string parameter passed in
function getQueryStringValue (key) {  
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
  }  

window.onload = function(){

	ajaxGetList();

}

function ajaxGetList(){

	console.log("starting the ajax request");
	
	fetch(URL)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					reimbDOMManipulation(secondResponse);
				})
}

function reimbDOMManipulation(listJSON){

    //set values of the form elements
    document.getElementById('listId').value = listJSON.listId;
    document.getElementById('groceryList').value = listJSON.listName;
        

}