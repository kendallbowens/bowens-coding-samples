import { shuffleArray } from "./utils";

//used to create logic when fetching from the API

export type Question = {
    category: string,
    correct_answer: string,
    difficulty: string,
    incorrect_answers: string[],
    question: string,
    type: string
}

export type QuestionState = Question & { answers: string[] };

export enum Difficulty {
    EASY = "easy",
    MEDIUM = "medium",
    HARD = "hard"
}

export const fetchQuizQuestions = async (
    amount: number, 
    difficulty: Difficulty
    ) => {
    const endpoint = `https://opentdb.com/api.php?amount=${amount}&diffuculty=${difficulty}&type=multiple`;

    //awaits fetch THEN json
    const data = await (await fetch(endpoint)).json();
    console.log(data);

    return data.results.map((question: Question) => (
        {
            ...question,
            answers: shuffleArray([...question.incorrect_answers, question.correct_answer]),
        }
    ))

};