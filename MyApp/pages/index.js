import React from "react";
import axios from "axios";

class Index extends React.Component {
  static async getInitialProps() {
    // return ({
    //     time: new Date().toISOString()
    // })
    // const promise = new Promise((resolve, reject) =>
    //   setInterval(() => {
    //     resolve({
    //       time: new Date().toISOString(),
    //     });
    //   }, 3000)
    // );
    // return promise;

    var promise = axios
      .get("http://localhost:4000/speakers")
      .then((response) => {
        return {
          hasErrored: false,
          speakerData: response.data,
        };
      })
      .catch((error) => {
        return {
          hasErrored: true,
          message: error.message,
        };
      });
    return promise;
  }

  constructor(props) {
    super(props);
    console.log("Index constructor called");
    this.state = {
      //   time: props.time,
      hasErrored: props.hasErrored,
      message: props.message,
      speakerData: props.speakerData,
    };
  }

  //   tick() {
  //     this.setState(() => {
  //       return {
  //         time: new Date().toLocaleString(),
  //       };
  //     });
  //   }

  componentDidMount() {
    // this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    // clearInterval(this.interval);
  }

    render() {
        return( 
        <ul>
            {this.state.speakerData.map ((speaker) =>
            <li key={speaker.id}>
                {speaker.firstName} {speaker.lastName}
            </li>)}
        </ul>
        )}
}

export default Index;
