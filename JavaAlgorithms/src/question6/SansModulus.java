package question6;

public class SansModulus {
	
 static boolean checkEven (int num) {
	 
	 if((num & 1) == 1) {
		 return false;
	 }
	 else {
		 return true;
	 }
	 
 }
 
static boolean checkEven2 (int num) {
	 
	 if(((num / 2) * 2) == num) {
		 return true;
	 }
	 else {
		 return false;
	 }
	 
 }
  
  
 // program starts here
 public static void main(String args[])
 {
      
     int n = 809;
     if(checkEven2(n))
         System.out.println("Even");
     else
         System.out.println("Odd");
      
 	}
}