package question9;

import java.util.ArrayList;
import java.util.List;

public class Primes {

	public static void hundredPrimes() {

		List<Integer> primeList = new ArrayList<>();
		List<Integer> returnList = new ArrayList<>();

		// storing numbers 1 to 100
		for (int i = 1; i <= 100; i++) {
			primeList.add(i);
		}

		for (int item : primeList) {
			
			boolean flag = (item == 0 || item == 1) ? false : true;

			if (item != 0 || item != 1) {

				int value = item / 2;

				for (int i = 2; i <= value; i++) {
					if (item % i == 0) {
						flag = false;
						break;
					}
				}
				
				if(flag == true) {
					returnList.add(item);
				}
				
			}

		}

		System.out.println(returnList);
	}

	public static void main(String[] args) {


		hundredPrimes();
	}

}
