package question14;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class SwitchCase {

	public static void myChoices(String choice, int num, String str) {

		switch (choice) {

		case "Square":
			System.out.println("The square root of the number is: " + Math.sqrt(num));
			break;

		case "Date":
			System.out.println(LocalDate.now());
			break;

		case "String":

			String[] arrOfStr = str.split(" ");

			for (String str2 : arrOfStr) {

				System.out.println(str2);
			}

		}

	}

	public static void main(String[] args) {

		myChoices("String", 4, "I am learning Core Java");

	}

}
