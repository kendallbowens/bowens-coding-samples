package question11;

import question11again.Class2;

public class Class1 {
	
	public static void main(String[] args) {
		
		//calling class
		Class2 cls2 = new Class2();
		
		//printing variables
		System.out.println(cls2.float1);
		System.out.println(cls2.float2);
	}
}
