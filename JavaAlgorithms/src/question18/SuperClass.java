package question18;

public abstract class SuperClass {
	
	public abstract boolean CountUppercase(String str);
	
	public abstract String LowerUpper(String str);
	
	public abstract int StringToInt(String str);
	
	
}
