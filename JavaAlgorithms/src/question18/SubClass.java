package question18;

public class SubClass extends SuperClass {

	@Override
	public boolean CountUppercase(String str) {
		
		char[] charArray = str.toCharArray();
		
		for(int i = 0; i < charArray.length; i++) {
			
			if(Character.isUpperCase(charArray[i]))
			
					return true;
		}
			
		return false;
	}

	@Override
	public String LowerUpper(String str) {
		
		String upperString = str.toUpperCase();
		
		return upperString;
	}

	@Override
	public int StringToInt(String str) {
		
		byte[] byteArray = str.getBytes();
		
		int newResult = byteArray.length + 10;
		
		return newResult;
	}

}
