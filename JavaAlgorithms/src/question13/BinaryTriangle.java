package question13;

public class BinaryTriangle {

	public static void rightTriangle(int numRows) {

		int value1 = 1;
		int row, col;

		// loop for number of rows
		for (row = 1; row <= numRows; row++) {
			
			StringBuilder rowString = new StringBuilder("");
			
			/// loop for number of columns
			for (col = 1; col <= row; col++) {
				
				value1 = (value1 == 1) ? 0 : 1;
				
				rowString.append(value1 + " ");
			}
			
			System.out.println(rowString);

		}

	}

	public static void main(String[] args) {
		rightTriangle(4);
	}

}
