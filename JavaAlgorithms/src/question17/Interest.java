package question17;

import java.util.Scanner;

public class Interest {

	public static void letsBank(double principal, double rate, double time) {
		
		double interest = principal * rate * time;
		
		System.out.println("Total interest is: " + interest);

	}

	public static void main(String[] args) {
		// new scanner object
		Scanner myDoub = new Scanner(System.in);
		
		System.out.println("Enter principal:");
		Double principal = myDoub.nextDouble();
		
		System.out.println("Enter rate:");
		Double rate = myDoub.nextDouble();

		System.out.println("Enter time:");
		Double time = myDoub.nextDouble();
		
		letsBank(principal, rate, time);
		myDoub.close();

	}
}
