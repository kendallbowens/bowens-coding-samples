package question5;

public class Substring {
	
	private static String myStringyMethod(String str, int idx) {
	
		
		byte[] strAsByteArray = str.getBytes();
		
		if (idx < 1) {
			return null;
		}
        byte[] result = new byte[idx];
        
        
        for(int i = 0; i < idx; i++)
        {
        	
        	if(i  > strAsByteArray.length-1) {
        		break;
        	}
        	result[i] = strAsByteArray[i];
        	
        	
        }
        
        return new String(result);
		
		
	}
	
	public static void main(String[] args) {
		System.out.println(myStringyMethod("Hello", 4));
		
		
	}

}
