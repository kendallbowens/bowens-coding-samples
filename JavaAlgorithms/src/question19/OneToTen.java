package question19;

import java.util.ArrayList;
import java.util.List;

public class OneToTen {

	public static void myNums(List<Integer> numList) {

		List<Integer> primeList = new ArrayList<>();

		for (int item : numList) {

			boolean flag = (item == 0 || item == 1) ? false : true;

			if (item != 0 || item != 1) {

				int value = item / 2;

				for (int i = 2; i <= value; i++) {
					if (item % i == 0) {
						flag = false;
						break;
					}
				}
			}

			if (flag) {
				primeList.add(item);
			}
		}

		for (int item : primeList) {

			numList.remove(numList.indexOf(item));

		}

		System.out.println(numList);
	}

	public static void main(String[] args) {

		ArrayList<Integer> intList = new ArrayList();

		for (int i = 1; i <= 10; i++) {
			intList.add(i);
		}

		System.out.println(intList);

		int evenSum = 0;
		int oddSum = 0;

		for (int n : intList) {

			if (n % 2 == 0) {
				evenSum += n;
			}

			else {
				oddSum += n;
			}
		}

		System.out.println(evenSum);
		System.out.println(oddSum);

		myNums(intList);

	}

}
