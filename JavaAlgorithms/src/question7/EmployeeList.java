package question7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeList {
	
	public static void main(String[] args) {
		
		List<Employee> myEmployees = new ArrayList<>();
		
		myEmployees.add(new Employee("Kendall", "Mathematics", 33));
		myEmployees.add(new Employee("Yaton", "Computer Science", 26));
	
		
		Collections.sort(myEmployees, new EmployeeNameComparator());
		System.out.println(myEmployees);
		Collections.sort(myEmployees, new EmployeeDeptComparator());
		System.out.println(myEmployees);
		Collections.sort(myEmployees, new EmployeeAgeComparator());
		System.out.println(myEmployees);


		
	}

	
	

}
