package question7;

public class Employee implements Comparable<Employee> {
	
	 private String name;
	 private String dept;
	 private int age;
	
	/*
	 * no arg constructor
	 * all arg constructor
	 * getter/setter
	 * toString
	 */
	
	public Employee(){
		
	}
	
	public Employee (String name, String dept, int age) {
		super();
		this.name = name;
		this.dept = dept;
		this.age = age;
	}


	@Override
	public int compareTo(Employee o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public String toString() {
		return "\nEmployee [name = " + name + ", dept = " + dept + ", age = " + age + "]";
	}

	public int getAge() {
		// TODO Auto-generated method stub
		return age;
	}

	public String getDept() {
		// TODO Auto-generated method stub
		return dept;
	}
	
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	

}
