package question7;

import java.util.Comparator;

public class EmployeeDeptComparator implements Comparator<Employee> {

	@Override
	public int compare(Employee o1, Employee o2) {
		// TODO Auto-generated method stub
		
		String o1Dept = o1.getDept();
		String o2Dept = o2.getDept();
		
		if(o1Dept.charAt(0)>o2Dept.charAt(0))
			return 1;
		else if(o2Dept.charAt(0)>o1Dept.charAt(0))
			return -1;
		else
			return 0;
	}

}
