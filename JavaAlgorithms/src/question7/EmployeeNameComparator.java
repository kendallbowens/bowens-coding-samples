package question7;

import java.util.Comparator;

public class EmployeeNameComparator  implements Comparator<Employee> {

	@Override
	public int compare(Employee o1, Employee o2) {
		// TODO Auto-generated method stub
		
		String o1Name = o1.getName();
		String o2Name = o2.getName();
		
		if(o1Name.charAt(0)>o2Name.charAt(0))
			return 1;
		else if(o2Name.charAt(0)>o1Name.charAt(0))
			return -1;
		else
			return 0;
	}

}
