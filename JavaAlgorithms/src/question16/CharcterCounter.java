package question16;

import java.util.Scanner;

public class CharcterCounter {

	public static void main(String[] args) {

		// create scanner object
		Scanner myStr = new Scanner(System.in);
		System.out.println("Enter string:");

		// read user input
		String str = myStr.nextLine();

		// Displays the total number of characters present in the given string
		System.out.println("Total number of characters in a string: " + str.length());

	}

}
