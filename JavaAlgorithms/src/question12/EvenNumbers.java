package question12;

public class EvenNumbers {

	public static void main(String[] args) {

		int nums[] = new int[100];

		// storing numbers 1 to 100
		for (int i = 0; i < 100; i++) {
			nums[i] = i+1;
		}

		for (int item : nums) {

			if (item % 2 == 0) {
				System.out.print(item + " ");

			}
		}

	}
}
