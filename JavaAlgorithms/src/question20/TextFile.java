package question20;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TextFile {
	
	public static void main(String[] args) throws IOException {
		
		File file = new File ("/Users/kendallbowens/Documents/GitRepos/2105-may24-java/kendall_bowens_code/question20.txt");
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String str = reader.readLine();
		
		while(str != null) {
			String tokens[] = str.split(":");
			
			System.out.println("Name: " + tokens[0] + tokens[1]);
	        System.out.println("Age: " + tokens[2] + " years");
	        System.out.println("State: " + tokens[3] + " State\n");
		
	        str = reader.readLine();
		}
		reader.close();
	}
	
	

}
