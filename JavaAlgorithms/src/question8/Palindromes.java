package question8;

import java.util.ArrayList;
import java.util.List;
import java.lang.*;
import java.io.*;
import java.util.*;

public class Palindromes {
	

	public static void myPalindromes(List<String> arr) {
		
		///created palindrome list
		List<String> palinList = new ArrayList<>();
		
		for(String str : arr) {
			
			StringBuilder sb = new StringBuilder(str);
			String revString = sb.reverse().toString();
			
			if(str.equals(revString)){
				palinList.add(str);
				
			}
		}
		
		System.out.println(palinList);
		

	}
	
	public static void main(String[] args) {
		List<String> stringList = new ArrayList<>();
		stringList.add("karan");
		stringList.add("madam");
		stringList.add("tom");
		stringList.add("civic");
		stringList.add("radar");
		stringList.add("sexes");
		stringList.add("jimmy");
		stringList.add("kayak");
		stringList.add("john");
		stringList.add("refer");
		stringList.add("billy");
		stringList.add("did");
		
		
		myPalindromes(stringList);
		
	}

}
