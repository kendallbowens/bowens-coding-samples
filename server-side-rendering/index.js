// const rootElement = document.getElementById('app');

/* Plain JavaScript */
// const myElement = document.createElement('h1');
// myElement.className = "orange";
// myElement.innerText = 'Hello from KB';
// rootElement.appendChild(myElement); 

/* React */
// const myReactElement = React.createElement(
//   'h1',
//   { className: 'orange' },
//   'Hello from KB and React'
// );
// ReactDOM.render(
//     myReactElement,
//     document.getElementById('app')
// );

/* React Functional Component + props */
// const Hello = function (props) {
//     return React.createElement(
//           'h1',
//           {
//               className: 'orange'
//           },
//           'Hello from KB and React ' + props.time
//     )};

// ReactDOM.render(
//     React.createElement(Hello, {time: new Date().toLocaleDateString()}, null),
//     document.getElementById('app')
// );

/* Class Component */
class Hello extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            time: new Date().toLocaleDateString()
        }
    }

    tick() {
        this.setState(() => {
            return ({
                time: new Date().toLocaleString()
            });
        });
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
    

    componentDidMount() {
        this.interval = setInterval (() => this.tick(), 1000)
    }
    

    render(){
        return React.createElement('h1',
        { className: 'orange'},
        'Hello from KB and React ' + this.state.time)
    }
};

ReactDOM.render(
    React.createElement(Hello, {time: new Date().toLocaleDateString()}, null),
    document.getElementById('app')
)





