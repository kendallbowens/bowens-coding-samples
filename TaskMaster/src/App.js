import React from 'react';
import { useState } from "react"; 
import Header from './components/Header';
import Tasks from './components/Tasks';
import AddTask from './components/AddTask';


function App() {

  const[showAddTask, setShowAddTask] = useState(false)

  const[tasks, setTasks] = useState([
    { 
    id: 1, 
    text: 'Gym', 
    day: 'Nov 26th at 9am', reminder: true
    },
    { 
      id: 2, 
      text: 'Trail Run', 
      day: 'Nov 26th at 11am', reminder: true
      },
    { 
      id: 3, 
      text: 'Interview', 
      day: 'Nov 29th at 2pm', reminder: true
    }
    ])

    //Add Task
    const addTask = (task) => {
      const id = Math.floor(Math.random()*10000) + 1

      const newTask = {id, ...task}

      setTasks([...tasks, newTask])
    }
    

    //Delete Task
    const deleteTask = (id)=>{
      setTasks(tasks.filter((task)=>task.id !== id))
    }

    //Toggle Reminder
    const toggleReminder =(id) => {
       console.log(id);

       setTasks(tasks.map((task)=> task.id === id ? {...task, reminder: !task.reminder} : task))
    }

  return (
    <div className='container'>
      <Header onAdd={()=> setShowAddTask(!showAddTask)} showAdd={showAddTask}/>
      {showAddTask && <AddTask onAdd={addTask}/>}
      {tasks.length > 0 ? < Tasks tasks={tasks}
        onDelete={deleteTask} onToggle={toggleReminder}/> : 'No Tasks To Show'}
    </div>
  );
}



export default App;
