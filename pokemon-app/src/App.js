import React, { useState, useEffect } from "react";
import PokemonList from "./components/PokemonList";
import { Pagination } from "./components/Pagination";
import axios from "axios";
import { BrowserRouter as Router} from 'react-router-dom';
import PokeSearch from "./components/PokeSearch";

function App() {
  const [pokemon, setPokemon] = useState([]);
  const [currentPageUrl, setCurrentPageUrl] = useState(
    "https://pokeapi.co/api/v2/pokemon/"
  );
  const [nextPageUrl, setNextPageUrl] = useState();
  const [prevPageUrl, setPrevPageUrl] = useState();
  const [loading, setLoading] = useState(true);
  


  useEffect(() => {
    setLoading(true);
    let cancel;
    axios
      .get(currentPageUrl, {
        cancelToken: new axios.CancelToken((c) => (cancel = c)),
      })
      .then((res) => {
        setLoading(false);
        setNextPageUrl(res.data.next);
        setPrevPageUrl(res.data.previous);
        setPokemon(res.data.results.map((p) => p.name));
      });
    return () => cancel();
  }, [currentPageUrl]);

  function goToNextPage() {
    setCurrentPageUrl(nextPageUrl);
  }

  function goToPrevPage() {
    setCurrentPageUrl(prevPageUrl);
  }


  if (loading) return "Loading...";

  return (
    <Router>
      <PokeSearch/>
      <PokemonList pokemon={pokemon} />
      <Pagination 
      goToNextPage={nextPageUrl ? goToNextPage : null} 
      goToPrevPage={prevPageUrl ? goToPrevPage : null} 
      />
    </Router>
  );
}

export default App;
