import React from "react";

const PokemonList = ({ pokemon }) => {

    return (
      <div>
        {pokemon.map((p) => (
          <li key={p}>{p}</li>
        ))}
      </div>
    );
  };

export default PokemonList;
