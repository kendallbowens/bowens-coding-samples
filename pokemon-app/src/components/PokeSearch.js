import React from 'react'

const PokeSearch = () => {
    return (
        <form action="/" method="get">
        <label htmlFor="header-search">
            <span className="visually-hidden">Search Pokemon </span>
        </label>
        <input
            type="text"
            id="header-search"
            placeholder="Search Pokemon"
            name="s" 
        />
        <button type="submit">Search</button>
    </form>
    )
}

export default PokeSearch
