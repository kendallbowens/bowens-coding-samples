package challenge.dao;

import java.util.List;

import challenge.model.Home;

public interface HomeDaoInterface {
	
	public void insert(Home home);
	public void update(Home home);
	public Home selectById (int id);
	public void delete(int id);
	public List<Home> selectAll();

}
