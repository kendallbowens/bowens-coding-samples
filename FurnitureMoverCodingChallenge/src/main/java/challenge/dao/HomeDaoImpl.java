package challenge.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import challenge.model.Home;

@Transactional
@Repository("homeDao")
public class HomeDaoImpl implements HomeDaoInterface {
	
	private SessionFactory sesFact;
	
	@Override
	public void insert(Home home) {
		sesFact.getCurrentSession().save(home);
	}

	@Override
	public Home selectById(int id) {
		System.out.println(id);
		return sesFact.getCurrentSession().get(Home.class, id);
	}

	@Override
	public List<Home> selectAll() {
		return sesFact.getCurrentSession().createQuery("from Home", Home.class).getResultList();
	}
	
	@Override
	public void update(Home home) {
		sesFact.getCurrentSession().merge(home);
		 
	}
	@Override
	public void delete(int id) {
		Home home = new Home();
		
		home.setHomeId(id);
		sesFact.getCurrentSession().delete(home);
	
	}

	public HomeDaoImpl() {
		
	}
	
	public HomeDaoImpl(SessionFactory sesFact) {
		super();
		this.sesFact = sesFact;
	}

	public SessionFactory getSesFact() {
		return sesFact;
	}

	@Autowired
	public void setSesFact(SessionFactory sesFact) {
		this.sesFact = sesFact;
	}


	
	
	
	
	

}
