package challenge.dao;

import java.util.List;

import challenge.model.Furniture;

public interface FurnitureDaoInterface {
	
	public void insert(Furniture furn);
	public void update(Furniture furn);
	public void delete(int id);
	public Furniture selectById (int id);
	public List<Furniture> selectAll(int homeId);

}
