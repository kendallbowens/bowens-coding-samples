package challenge.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import challenge.model.Furniture;
import challenge.model.Home;

@Transactional
@Repository("furnDao")
public class FurnitureDaoImpl implements FurnitureDaoInterface {

	
	private SessionFactory sesFact;
	
	
	@Autowired
	public FurnitureDaoImpl(SessionFactory sesFact) {
		super();
		this.sesFact = sesFact;
	}

	@Override
	public void insert(Furniture furn) {
		sesFact.getCurrentSession().save(furn);

	}

	@Override
	public void update(Furniture furn) {
		sesFact.getCurrentSession().merge(furn);
	}

	@Override
	public Furniture selectById(int id) {
		return sesFact.getCurrentSession().get(Furniture.class, id);
	}

	@Override
	public void delete(int id) {
		Furniture furn = new Furniture();
		
		furn.setFurnId(id);
		sesFact.getCurrentSession().delete(furn);
		
	}
	@Override
	public List<Furniture> selectAll(int homeId) {
		System.out.println(homeId);
		return sesFact.getCurrentSession().createQuery("from Furniture where homeId =: currentHomeId", Furniture.class)
				.setParameter("currentHomeId", homeId)
				.list();
	}


}
