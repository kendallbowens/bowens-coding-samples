package challenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import challenge.model.Home;
import challenge.service.HomeServiceInterface;

@Controller
@RequestMapping(value="/home")
public class HomeController {
	
	private HomeServiceInterface homeServ;

	@Autowired
	public HomeController(HomeServiceInterface homeServ) {
		super();
		this.homeServ = homeServ;
		insertValues(homeServ);
	}
	
	private void insertValues(HomeServiceInterface homeServ) {
		homeServ.addHome(new Home(1200, "Apartment", null));
		homeServ.addHome(new Home(10000, "Suburbs", null));
		
	}
	
	@GetMapping(value="/allHomes")
	public @ResponseBody List<Home> allHomes(){
		System.out.println("In get all homes");
		return homeServ.getAllHomes();
	}
	
	@PostMapping(value="/homelist")
	public String addHome(Home home) {
		System.out.println("Adding a home");
		homeServ.addHome(home);
		
		String url = "/FurnitureMoverCodingChallenge/index.html";
		
		return "redirect: " + url;
	}
	
	@PostMapping(value="/updateHome")
	public String updateHome(@RequestParam("homeId")int id
				, @RequestParam("homeDescription") String desc
				, @RequestParam("homeSize") double size)	 {
		System.out.println("updating home");
		
		homeServ.updateHome(new Home(id, size, desc));
		
		return "/index.html";
	}
	
	@GetMapping(value="/getHomeById")
	public @ResponseBody Home getHomeById(@RequestParam("homeId")int id) {
		
		System.out.println(id);
		
		return homeServ.getById(id);
		
	}
	
	@GetMapping(value="/delete")
	private String deleteHome(@RequestParam("homeId")int id) {
		homeServ.removeHome(id);
		
		return "/index.html";
	}
	
	


}
