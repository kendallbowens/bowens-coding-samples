package challenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import challenge.model.Furniture;
import challenge.model.Home;
import challenge.service.FurnServiceInterface;
import challenge.service.HomeServiceImpl;
import challenge.service.HomeServiceInterface;

@Controller
@RequestMapping(value="/furn")
public class FurnitureController {
	
	private FurnServiceInterface furnServ;
	
	@Autowired
	public FurnitureController(FurnServiceInterface furnServ) {
		super();
		this.furnServ = furnServ;
		insertValues(furnServ);
	}
	
	private void insertValues(FurnServiceInterface furnServ) {
		furnServ.addFurniture(new Furniture("couch", 50, 1));
		furnServ.addFurniture(new Furniture("table", 30, 2));
		
	}
	
	@GetMapping(value="/allFurn")
	public @ResponseBody List<Furniture> allFurn(@RequestParam("homeId")int homeId){
		System.out.println("In get all furniture");
		return furnServ.getAllFurniture(homeId);
	}
	
	@PostMapping(value="/furnlist")
	public String addFurn(Furniture furn) {
		
		System.out.println(furn);
		
		System.out.println("Adding furniture");
		boolean addFurn = furnServ.addFurniture(furn);
		
		System.out.println(addFurn);
		
		String url;
		
		if(addFurn) {
			url = "/FurnitureMoverCodingChallenge/resources/html/furniture.html";
			
		}
		else {
			url = "/FurnitureMoverCodingChallenge/resources/html/add-furn.html";
		}
		
		return "redirect: " + url;
	}
	
	@GetMapping(value="/delete")
	private String deleteHome(@RequestParam("furnId")int id) {
		furnServ.removeFurniture(id);
		
		return "/resources/html/furniture.html";
	}
	
	@PostMapping(value="/updateFurn")
	public String updateHome(@RequestParam("furnId")int id
				, @RequestParam("furnDescription") String desc
				, @RequestParam("furnSize") double size
				, @RequestParam("homeId") int homeId)	 {
		System.out.println("updating home");
		
		furnServ.updateFurniture(new Furniture(id, desc, size, homeId));
		
		return "/index.html";
	}

}
