package challenge.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="home_furniture")
public class Furniture {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="furniture_id")
	private int furnId;
	
	@Column(name="furniture_description")
	private String furnDescription;

	@Column(name="furniture_size")
	private double furnSize;
	
	@Column(name="home_id")
	private int homeId;
	
	public Furniture() {
		
	}
	
	public Furniture(String furnDescription, double furnSize, int homeId) {
		super();
		this.furnDescription = furnDescription;
		this.furnSize = furnSize;
		this.homeId = homeId;
	}

	
	

	public Furniture(int furnId, String furnDescription, double furnSize, int homeId) {
		super();
		this.furnId = furnId;
		this.furnDescription = furnDescription;
		this.furnSize = furnSize;
		this.homeId = homeId;
	}

	public int getFurnId() {
		return furnId;
	}

	public void setFurnId(int furnId) {
		this.furnId = furnId;
	}

	public String getFurnDescription() {
		return furnDescription;
	}

	public void setFurnDescription(String furnDescription) {
		this.furnDescription = furnDescription;
	}

	public double getFurnSize() {
		return furnSize;
	}

	public void setFurnSize(double furnSize) {
		this.furnSize = furnSize;
	}
	

	public int getHomeId() {
		return homeId;
	}

	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}

	@Override
	public String toString() {
		return "Furniture [furnId=" + furnId + ", furnDescription=" + furnDescription + ", furnSize=" + furnSize
				+ ", homeId=" + homeId + "]";
	}

	
	
	
	
	
}
