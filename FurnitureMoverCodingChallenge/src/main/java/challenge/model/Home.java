package challenge.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="home_table")
public class Home {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="home_id")
	private int homeId;
	
	@Column(name="home_size", nullable=false)
	private double homeSize;
	
	@Column(name="home_description")
	private String homeDescription;
	
	@OneToMany(fetch=FetchType.EAGER)
	private List<Furniture> furnList;
	
	public Home() {
		
	}
	
	
	public Home(int homeId, double homeSize, String homeDescription) {
		super();
		this.homeId = homeId;
		this.homeSize = homeSize;
		this.homeDescription = homeDescription;
	}



	public Home(double homeSize, String homeDescription, List<Furniture> furnList) {
		super();
		this.homeSize = homeSize;
		this.homeDescription = homeDescription;
		this.furnList = furnList;
	}

	public Home(int homeId, double homeSize, String homeDescription, List<Furniture> furnList) {
		super();
		this.homeId = homeId;
		this.homeSize = homeSize;
		this.homeDescription = homeDescription;
		this.furnList = furnList;
	}

	public int getHomeId() {
		return homeId;
	}

	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}

	public double getHomeSize() {
		return homeSize;
	}

	public void setHomeSize(double homeSize) {
		this.homeSize = homeSize;
	}

	public String getHomeDescription() {
		return homeDescription;
	}

	public void setHomeDescription(String homeDescription) {
		this.homeDescription = homeDescription;
	}

	public List<Furniture> getFurnList() {
		return furnList;
	}

	public void setFurnList(List<Furniture> furnList) {
		this.furnList = furnList;
	}

	@Override
	public String toString() {
		return "\n\tHome [homeId=" + homeId + ", homeSize=" + homeSize + ", homeDescription=" + homeDescription
				+ ", furnList=" + furnList + "]";
	}
	
	
	
	
}
