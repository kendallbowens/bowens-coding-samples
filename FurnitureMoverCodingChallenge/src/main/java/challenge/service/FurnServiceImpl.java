package challenge.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import challenge.dao.FurnitureDaoInterface;
import challenge.dao.HomeDaoImpl;
import challenge.model.Furniture;
import challenge.model.Home;

@Service
public class FurnServiceImpl implements FurnServiceInterface{

	@Autowired
	FurnitureDaoInterface furnDao;
	Home home = new Home();
	
	@Override
	public boolean addFurniture(Furniture furn) {
		
		//get the home id
		int homeId = furn.getHomeId();
		List<Furniture> furnList = furnDao.selectAll(homeId);
		
		//get sum of size of items
		double totalSize = 0;
		for(Furniture item : furnList) {
			totalSize += item.getFurnSize();
		}
		
		totalSize += furn.getFurnSize();
		
		HomeServiceImpl homeServ = new HomeServiceImpl();
		
		Home currentHome = homeServ.getById(1);
		
		//System.out.println(currentHome.getFurnList());
		
		//Home currentHome = new Home(1,12,"");
		
		if(currentHome.getFurnList() != null && (currentHome.getHomeSize() / 2) < totalSize) {
			return false;
		}
		else {
			furnDao.insert(furn);
			return true;
		}
	
	}

	@Override
	public void updateFurniture(Furniture furn) {
		furnDao.update(furn);
	}

	@Override
	public Furniture getById(int id) {
		return furnDao.selectById(id);
	}

	@Override
	public List<Furniture> getAllFurniture(int homeId) {
		return furnDao.selectAll(homeId);
	}

	@Override
	public void removeFurniture(int id) {
		furnDao.delete(id);
		
	}

}
