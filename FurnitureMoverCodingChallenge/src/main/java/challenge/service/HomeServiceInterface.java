package challenge.service;

import java.util.List;

import challenge.model.Home;

public interface HomeServiceInterface {
	
	public void addHome(Home home);
	public void updateHome(Home home);
	public Home getById (int id);
	public void removeHome(int id);
	public List<Home> getAllHomes();

}
