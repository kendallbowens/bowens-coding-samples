package challenge.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import challenge.dao.HomeDaoImpl;
import challenge.dao.HomeDaoInterface;
import challenge.model.Home;

@Service
public class HomeServiceImpl implements HomeServiceInterface {

	@Autowired
	HomeDaoInterface homeDao;
	
	@Override
	public void addHome(Home home) {
		homeDao.insert(home);

	}

	@Override
	public Home getById(int id) {
		System.out.println(id);
		return homeDao.selectById(id);
	}

	@Override
	public List<Home> getAllHomes() {
		return homeDao.selectAll();
	}

	@Override
	public void updateHome(Home home) {
		homeDao.update(home);
	}

	@Override
	public void removeHome(int id) {
		homeDao.delete(id);
		
	}

}
