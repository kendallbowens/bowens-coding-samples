package challenge.service;

import java.util.List;

import challenge.model.Furniture;
import challenge.model.Home;

public interface FurnServiceInterface {
	
	public boolean addFurniture(Furniture furn);
	public void updateFurniture(Furniture furn);
	public void removeFurniture(int id);
	public Furniture getById (int id);
	public List<Furniture> getAllFurniture(int homeId);

}
