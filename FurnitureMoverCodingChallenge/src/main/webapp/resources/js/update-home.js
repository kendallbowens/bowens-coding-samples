console.log("in this js file");

let URL = 'http://localhost:9009/FurnitureMoverCodingChallenge/api/home/getHomeById';

let homeId = getQueryStringValue('homeId'); //replace with passed in id
URL = URL + '?homeId=' + homeId;

//found function to parse the query string parameter passed in
function getQueryStringValue (key) {  
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
  }  

window.onload = function(){

	ajaxGetHome();

}

function ajaxGetHome(){

	console.log("starting the ajax request");
	
	fetch(URL)
		.then(function(response){
					console.log("in first response")
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					homeDOMManipulation(secondResponse);
				})

}

function homeDOMManipulation(homeJSON){

console.log("inserting values");
	
	
    //set values of the form elements
    document.getElementById('homeId').value = homeJSON.homeId;
    document.getElementById('homeDescription').value = homeJSON.homeDescription;
    document.getElementById('homeSize').value = homeJSON.homeSize;
 
 }
