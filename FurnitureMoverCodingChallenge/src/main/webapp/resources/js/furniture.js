console.log("in js file");

let URL = 'http://localhost:9009/FurnitureMoverCodingChallenge/api/furn/allFurn'

let homeId = getQueryStringValue('homeId'); //replace with passed in id
URL = URL + '?homeId=' + homeId;

document.getElementById('addButton').href = 
	document.getElementById('addButton').href + "?homeId=" + homeId;

//found function to parse the query string parameter passed in
function getQueryStringValue (key) {  
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
  }

console.log(URL);


window.onload = function(){
    console.log(URL);
	ajaxViewAllHomes();
}

function ajaxViewAllHomes(){
	
	console.log("starting the ajax request");
	
	
	fetch(URL)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					furnDOMManipulation(secondResponse);
				})
}



function furnDOMManipulation(furnJSON){

	
	for(let i=0; i < furnJSON.length ; i++){
		
		let newTR = document.createElement("tr");
		let newTH = document.createElement("th");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		let newTD4 = document.createElement("td");
	
		
		
		newTH.setAttribute("scope", "row");
		let myTextH = document.createTextNode(furnJSON[i].furnId);
        let myTextD1 = document.createTextNode(furnJSON[i].furnDescription);
        let myTextD2 = document.createTextNode(furnJSON[i].furnSize);


        //UPDATE button 
        let newLink = document.createElement('a');
        let linkButton = document.createElement("button");
        linkButton.innerHTML = "Update";
        linkButton.className = 'updateButton';
        newLink.appendChild(linkButton);
        newLink.title = 'Update';
        newLink.href = 'http://localhost:9009/FurnitureMoverCodingChallenge/resources/html/update-furn.html?furnId=' + furnJSON[i].furnId
        
     	//DELETE BUTTON 
		let newDelete = document.createElement('a');
        let deleteButton = document.createElement("button");
        deleteButton.innerHTML = "DELETE";
        deleteButton.className = 'deleteButton';
        newDelete.appendChild(deleteButton);
        newDelete.title = 'DELETE';
        newDelete.href = 'http://localhost:9009/FurnitureMoverCodingChallenge/api/furn/delete?furnId=' + furnJSON[i].furnId;


        let myItemD3 = newLink;
        let myItemD4 = newDelete;

        newTH.appendChild(myTextH);
        newTD1.appendChild(myTextD1);
        newTD2.appendChild(myTextD2);
        newTD3.appendChild(myItemD3);
        newTD4.appendChild(myItemD4);
       
        
		
        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(myItemD3);
        newTR.appendChild(myItemD4);
       

        let newSelection = document.querySelector("#myFurnTable").querySelector("#furnTableBody");
        newSelection.appendChild(newTR);
	}
}