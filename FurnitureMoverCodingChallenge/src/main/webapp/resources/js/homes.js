console.log("in js file");

let URL = 'http://localhost:9009/FurnitureMoverCodingChallenge/api/home/allHomes'

window.onload = function(){
    console.log(URL);
	ajaxViewAllHomes();
}

function ajaxViewAllHomes(){
	
	console.log("starting the ajax request");
	
	
	fetch(URL)
		.then(function(response){
					const convertedResponse = response.json();
					console.log(convertedResponse);
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					homeDOMManipulation(secondResponse);
				})

}



function homeDOMManipulation(homeJSON){
	
	for(let i=0; i < homeJSON.length ; i++){
		
		let newTR = document.createElement("tr");
		let newTH = document.createElement("th");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		let newTD4 = document.createElement("td");
		let newTD5 = document.createElement("td");
	
		
		
		newTH.setAttribute("scope", "row");
		let myTextH = document.createTextNode(homeJSON[i].homeId);
        let myTextD1 = document.createTextNode(homeJSON[i].homeDescription);
        let myTextD2 = document.createTextNode(homeJSON[i].homeSize);


        //UPDATE button 
        let newLink = document.createElement('a');
        let linkButton = document.createElement("button");
        linkButton.innerHTML = "Update";
        linkButton.className = 'updateButton';
        newLink.appendChild(linkButton);
        newLink.title = 'Update';
        newLink.href = 'http://localhost:9009/FurnitureMoverCodingChallenge/resources/html/update-home.html?homeId=' + homeJSON[i].homeId
        
        //VIEW FURNITURE ITEMS BUTTON
        let viewLink = document.createElement('a');
        let viewLinkButton = document.createElement("button");
        viewLinkButton.innerHTML = "View Items";
        viewLinkButton.className = 'viewButton';
        viewLink.appendChild(viewLinkButton);
        viewLink.title = 'View Items';
        viewLink.href = 'http://localhost:9009/FurnitureMoverCodingChallenge/resources/html/furniture.html?homeId=' + homeJSON[i].homeId
	
		//DELETE BUTTON 
		let newDelete = document.createElement('a');
        let deleteButton = document.createElement("button");
        deleteButton.innerHTML = "DELETE";
        deleteButton.className = 'deleteButton';
        newDelete.appendChild(deleteButton);
        newDelete.title = 'DELETE';
        newDelete.href = 'http://localhost:9009/FurnitureMoverCodingChallenge/api/home/delete?homeId=' + homeJSON[i].homeId;
		

        let myItemD3 = newLink;
        let myItemD4 = viewLink;
        let myItemD5 = newDelete;

        newTH.appendChild(myTextH);
        newTD1.appendChild(myTextD1);
        newTD2.appendChild(myTextD2);
        newTD3.appendChild(myItemD3);
        newTD4.appendChild(myItemD4);
        newTD5.appendChild(myItemD5);
       
        
		
        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(myItemD3);
        newTR.appendChild(myItemD4);
        newTR.appendChild(myItemD5);
       

        let newSelection = document.querySelector("#myHomeTable").querySelector("#homeTableBody");
        newSelection.appendChild(newTR);
	}
}