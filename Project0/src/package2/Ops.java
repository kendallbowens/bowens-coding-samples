package package2;

import java.util.ArrayList;
import java.util.List;

import package1.Account;
import package1.Fridge;
import package1.HealthInspector;
import package1.Restaurant;

public class Ops {

	List<Restaurant> restList = null;
	List<HealthInspector> healthList = null;
	Account currentAccount = null;
	Fridge currentFridge = null;
//	String foodItem = "";
	int maxItemsInFridge = 3;

	public Ops() {

		restList = new ArrayList<>();
		healthList = new ArrayList<>();

	}
	
	public Account getCurrentAcount() {
		return currentAccount;
	}

	public Fridge getCurrentFridge() {
		return currentFridge;
	}
	
	public String createAcct(String acctType, String acctName, String userName, String password) {

		if (acctType.equals("H")) {
			HealthInspector health1 = new HealthInspector(acctName, userName, password, "H");
			healthList.add(health1);

			return "The following Health Inspector has been created. " + health1.toString();
		} else {
			Restaurant rest1 = new Restaurant(acctName, userName, password, "R");
			restList.add(rest1);

			return "The following Restaurant has been created. " + rest1.toString();
		}

	}

	public boolean login(String userName, String password) {
		// call findAllAccounts
		List<Account> acctList = findAllAccounts();
		// loop thru accounts to find matching username and password
		for (Account item : acctList) {
			if (item.userName.equals(userName) && item.password.equals(password)) {
				currentAccount = item;
				return true;
			}
		}
		// did not find matching login
		return false;

	}

	public List<Account> findAllAccounts() {
		// declare variable for all accounts
		List<Account> acctList = new ArrayList<>();

		// add all restaurants

		for (Restaurant item : restList) {			
			acctList.add(new Account(item.acctName, item.userName, item.password, item.acctType));
		}
		
		// add all health inspectors
		for (HealthInspector item : healthList) {
			acctList.add(new Account(item.acctName, item.userName, item.password, item.acctType));
		}

		// return new list of all accounts
		return acctList;
	}

	public List<Fridge> findAllFridges(String acctName, String acctType) {
		// find account & account type
		Restaurant restaurant = null;
		HealthInspector health = null;

		if (acctType.equals("H")) {
			for (HealthInspector item : healthList) {
				if (item.acctName.equals(acctName)) {
					health = item;
					break;
				}
			}
			return health.fridgeList;
		} else {
			for (Restaurant item : restList) {
				if (item.acctName.equals(acctName)) {
					restaurant = item;
					break;
				}
			}
			return restaurant.fridgeList;
		}

	}

	public Account findAcctByUser(String username) {
		// get list of accounts
		List<Account> acctList = findAllAccounts();

		// loop thru accounts to find matching username
		for (Account item : acctList) {
			if (item.userName.equals(username)) {
				return item;
			}
		}
		return null;

	}

	private Restaurant getRestaurant(String restName) {

		Restaurant restaurant = null;

		for (Restaurant item : restList) {
			if (item.acctName.equals(restName)) {
				restaurant = item;
			}
		}
		return restaurant;
	}
	
	public Fridge addFridge(String restName, int fridgeNumber, String fridgeName) {
		// look up resturant (access restList)
		Restaurant restaurant = getRestaurant(restName);

		boolean fridgeExists = false;

		// search for fridge
		for (Fridge item : restaurant.fridgeList) {
			if (item.fridgeNumber == fridgeNumber) {
				fridgeExists = true;
			}
		}

		// add to fridgeList
		if (!fridgeExists) {
			// create new instance of fridge
			Fridge fridge = new Fridge(restName, fridgeNumber, fridgeName);
			restaurant.fridgeList.add(fridge);
			return fridge;
		}
		else {
			return null;
		}
	}

	public String removeFridge(String restName, int fridgeNumber, String fridgeName) {
		// look for restaurant
		Restaurant restaurant = getRestaurant(restName);
		String goneFood = "";

		boolean fridgeExists = false;
		int fridgeIdx = -1;

		// find fridge in the list
		for (int i = 0; i < restaurant.fridgeList.size(); i++) {
			Fridge item = restaurant.fridgeList.get(i);

			if (item.fridgeName.equals(fridgeName) || item.fridgeNumber == fridgeNumber) {
				fridgeIdx = i;
			}
		}

		// remove from fridge List
		if (fridgeIdx > -1) {
			Fridge item = restaurant.fridgeList.get(fridgeIdx);

			if (item != null) {
				// get list of food in fridge

				if (item.foodList != null) {
					for (String food : item.foodList) {
						boolean foodAdded = false;

						for (Fridge fridge : restaurant.fridgeList) {
							if (restaurant.fridgeList.size() < maxItemsInFridge) {
								this.addFood(restName, fridge.fridgeNumber, food);
								foodAdded = true;
								break;
							}

						}
						if (foodAdded == false)
							goneFood = goneFood + " " + food + " has been discarded.";
					}

				}
			}
			restaurant.fridgeList.remove(fridgeIdx);
			return "Refrigerator has been removed." + goneFood;
		} else {
			return "Refrigerator cannot be found.";
		}

	}

	public String addFood(String restName, int fridgeNumber, String foodItem) {
		// get restaurant
		Restaurant restaurant = getRestaurant(restName);

		// access fridge
		Fridge fridge = null;

		for (Fridge item : restaurant.fridgeList) {
			if (item.fridgeNumber == fridgeNumber) {
				fridge = item;
			}
		}

		if (fridge != null) {
			if (fridge.foodList == null) {
				// create instance of food list
				fridge.foodList = new ArrayList<>();
			}
			// food items cannot exceed maxItemsInFridge (if stmt.)
			if (fridge.foodList.size() < maxItemsInFridge) {
				fridge.foodList.add(foodItem);
			} else {
				return "Refrigerator is full.";
			}
			return foodItem + " has been added to " + fridgeNumber + ".";
		} else {
			return "Refrigerator not found.";
		}

	}

	public String removeFood(String restName, int fridgeNumber, String foodItem) {
		// go to restaurant
		Restaurant restaurant = getRestaurant(restName);

		// access fridge
		Fridge fridge = null;

		for (Fridge item : restaurant.fridgeList) {
			if (item.fridgeNumber == fridgeNumber) {
				fridge = item;
			}
		}

		int removeItem = -1;

		// remove items in food list
		if (fridge != null) {
			if (fridge.foodList != null) {
				for (int i = 0; i < fridge.foodList.size(); i++) {
					if (fridge.foodList.get(i).equals(foodItem)) {
						removeItem = i;
					}
				}
				if (removeItem > -1) {
					fridge.foodList.remove(removeItem);
				}
				else {
					return "Food not found.";
				}
			}

			return foodItem + " has been removed from " + fridgeNumber + ".";
		} else {
			return "Refrigerator not found.";
		}
	}

	public String moveFood(String restName, int sourceFridge, int destFridge, String foodItem) {
		// access restaurant
		Restaurant restaurant = getRestaurant(restName);

		// access source/destination fridge
		Fridge source = null;
		Fridge destination = null;

		for (Fridge item : restaurant.fridgeList) {
			if (item.fridgeNumber == sourceFridge) {
				source = item;
			}
			if (item.fridgeNumber == destFridge) {
				destination = item;
			}
		}

		String removeResult = this.removeFood(restName, sourceFridge, foodItem);
		String addResult = this.addFood(restName, destFridge, foodItem);
		
		if(addResult.equals("Refrigerator is full.")) {
			addResult +=  " " + foodItem + " was thrown away.";
		}
		
		return removeResult + " " + addResult;

	}


	public String addFridgeToInspector(String inspecAcctName, Fridge fridge) {
		// access inspector list
		//List<Account> inspList = findAllAccounts();// this.healthList;
		List<HealthInspector> inspList = this.healthList;
		//List<Fridge> fridgeList = new ArrayList<>();
		String result = "";
		
		// loop thru insp list
		for (HealthInspector item : inspList) {
			if (item.acctName.equals(inspecAcctName) && item.acctType.equals("H")) {
				if (item.fridgeList == null) {
					item.fridgeList = new ArrayList<>();
				}
				// assign fridge to inspector
				item.fridgeList.add(fridge);
				
				fridge.healthList.add(item);
				result = inspecAcctName + " now has access to " + fridge.fridgeName;
				
				break;
			}
			else
			{
				result = "Inspector not found.";
			}
		}
		return result;

	}

	public String removeFridgeFromInspector(String inspecAcctName, Fridge fridge ) {
		// access inspector list
		List<HealthInspector> inspList = this.healthList;

		// loop thru insp list
		for (HealthInspector item : inspList) {
			if (item.acctName.equals(inspecAcctName)) {
				if (item.fridgeList == null) {
					item.fridgeList = new ArrayList<>();
				}
				// remove fridge from inspector
				item.fridgeList.remove(fridge);
				fridge.healthList.remove(item);
				break;
			}
		}
		return inspecAcctName + " no longer has access to " + fridge.fridgeName ;

	}

	public static void persistAllData() {

	}

}
