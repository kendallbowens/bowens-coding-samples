package package2;

import java.util.List;
import java.util.Scanner;

import package1.Account;
import package1.Fridge;
import package1.HealthInspector;

import java.util.ArrayList;

public class UI {

	static Ops opsState = new Ops();
	static Scanner scanner = new Scanner(System.in);

	public static boolean login() {

		// show login menu
		scanner = new Scanner(System.in);
		System.out.println("Enter username: ");
		String userName = scanner.nextLine();

		System.out.println("Enter password: ");
		String password = scanner.nextLine();

		if (opsState.login(userName, password)) {
			return true;
		} else {
			System.out.println("Login not Successful.");
			return false;
		}

	}

	public static void registration() {

		scanner = new Scanner(System.in);
		System.out.println("Enter H for Health Inspector or R for Restaurant");
		String acctType = scanner.nextLine();

		String acctTypeLabel = (acctType.equals("H")) ? "Health Inspector" : "Restaurant";
		System.out.println("Enter " + acctTypeLabel + " name: ");
		String acctName = scanner.nextLine();

		System.out.println("Enter username: ");
		String userName = scanner.nextLine();

		System.out.println("Enter password: ");
		String password = scanner.nextLine();

		String result = opsState.createAcct(acctType, acctName, userName, password);

		System.out.println(result);
	}

	public static boolean menu3FridgeMenu() {

		boolean active = true;
		String menu1 = "1. List Food\n2. Add Food\n3. Remove Food\n4. Move Food\n5. Add Inspector\n6. Remove Inspector\n7. List Inspectors\n0. Return to Previous Menu";
		System.out.println(menu1);
		int menuOption = scanner.nextInt();
		scanner.nextLine();

		switch (menuOption) {

		case 1: // List Food
			System.out.println(opsState.currentFridge.toString());
			System.out.println("All Food Items: " + opsState.currentFridge.foodList.size());
			for (String item : opsState.currentFridge.foodList) {
				System.out.println(item);
			}
			break;
		case 2: // Add Food
			// if Inspector then don't allow

			if (opsState.currentAccount.acctType.equals("H")) {
				System.out.println("Inspectors cannot add food.");
			} else {
				System.out.println("Enter food name: ");
				String foodDesc = scanner.nextLine();
//				scanner.nextLine();

				String result = opsState.addFood(opsState.currentAccount.acctName, opsState.currentFridge.fridgeNumber,
						foodDesc);
				System.out.println(result);
			}
			break;
		case 3: // Remove Food
			System.out.println("List of food in refrigerator: ");

			for (String item : opsState.currentFridge.foodList) {
				System.out.println(item);
			}

			System.out.println("Enter food item:");
			String foodItem = scanner.nextLine();
			// remove food
			String result = "";
			if (opsState.currentAccount.acctType.equals("R")) {
				result = opsState.removeFood(opsState.currentAccount.acctName, opsState.currentFridge.fridgeNumber,
						foodItem);
			}
			else
			{
				result = opsState.removeFood(opsState.currentFridge.owner, opsState.currentFridge.fridgeNumber,
						foodItem);
			}
			System.out.println(result);

			break;
		case 4: // Move Food
			// list food
			if (opsState.currentAccount.acctType.equals("H")) {
				System.out.println("Inspectors cannot move food.");
			} else {
				List<Fridge> fridgeList = null;
	
				System.out.println("Total Food Items: " + opsState.currentFridge.foodList.size());
				for (String item : opsState.currentFridge.foodList) {
					System.out.println(item);
				}
				// move from source to dest
				System.out.println("Enter food name to remove from " + opsState.currentFridge.fridgeNumber + ":");
				String foodDesc = scanner.nextLine();
				
				// select fridge to add to
				fridgeList = opsState.findAllFridges(opsState.currentAccount.acctName, opsState.currentAccount.acctType);
				System.out.println("Total refrigerators: " + fridgeList.size());
				for (Fridge item : fridgeList) {
					if(!item.fridgeName.equals(opsState.currentFridge.fridgeName)) {
						System.out.println(item.fridgeNumber + "-" + item.fridgeName);
					}
				}
	
				System.out.println("Enter destination refrigerator number: ");
				int fridgeNumber = scanner.nextInt();
				scanner.nextLine();
	
	
				String moveResult = opsState.moveFood(opsState.currentAccount.acctName, opsState.currentFridge.fridgeNumber,
						fridgeNumber, foodDesc);
				System.out.println(moveResult);
			}

			break;
		case 5: // Add Inspector
			// list inspectors
			if (opsState.currentAccount.acctType.equals("H")) {
				System.out.println("Inspectors cannot add inspectors.");
			} else {
				System.out.println("Current Inspectors: " + opsState.healthList.size());
				for (HealthInspector item : opsState.healthList) {
					System.out.println(item.acctName);
				}
	
				System.out.println("Enter inspector to add: ");
				String healthInsp = scanner.nextLine();
				String result2 = opsState.addFridgeToInspector(healthInsp, opsState.currentFridge);
				System.out.println(result2);
			}
			break;
		case 6: // Remove Inspector
			if (opsState.currentAccount.acctType.equals("H")) {
				System.out.println("Inspectors cannot remove inspectors.");
			} else {
				System.out.println("Current Inspectors: " + opsState.currentFridge.healthList.size());
				for (HealthInspector item : opsState.currentFridge.healthList) {
					System.out.println(item.acctName);
				}
	
				System.out.println("Enter inspector to remove: ");
				String removeInsp = scanner.nextLine();
	
				String result3 = opsState.removeFridgeFromInspector(removeInsp, opsState.currentFridge);
				System.out.println(result3);
			}
			break;
		case 7: //List Inspectors
			System.out.println("Current Inspectors: " + opsState.currentFridge.healthList.size());
			for (HealthInspector item : opsState.currentFridge.healthList) {
				System.out.println(item.acctName);
			}
			break;
		case 0: // exit to main menu
			active = false;
			break;
		}
		return active;
	}

	public static boolean menu2Account() {

		boolean active = true;
		System.out.println("You are now logged in as Account: " + opsState.currentAccount.acctName);
		System.out.println("");
		System.out.println("Select item from menu.");
		System.out.println("");
		String menu2 = "1. List Fridges,2. Select Fridge,3. Add Fridge,4. Remove Fridge,0. Return to Main Menu";
		for (String item : menu2.split(",")) {
			System.out.println(item);
		}

		int menuOption = scanner.nextInt();
		scanner.nextLine();
		int fridgeNumber = -1;
		List<Fridge> fridgeList = null;

		switch (menuOption) {

		case 1: // List Fridge
			fridgeList = opsState.findAllFridges(opsState.currentAccount.acctName, opsState.currentAccount.acctType);
			System.out.println("Total refrigerators: " + fridgeList.size());
			for (Fridge item : fridgeList) {
				System.out.println(item.fridgeNumber + "-" + item.fridgeName);
			}
			break;

		case 2: // Select Fridge
			fridgeList = opsState.findAllFridges(opsState.currentAccount.acctName, opsState.currentAccount.acctType);
			System.out.println("Total refrigerators: " + fridgeList.size());
			for (Fridge item : fridgeList) {
				System.out.println(item.fridgeNumber + "-" + item.fridgeName);
			}

			System.out.println("Enter refrigerator number: ");
			fridgeNumber = scanner.nextInt();
			scanner.nextLine();

			for (Fridge item : fridgeList) {
				if (item.fridgeNumber == fridgeNumber) {
					opsState.currentFridge = item;
				}
			}
			if (opsState.currentFridge == null) {
				System.out.println("Refrigerator not found.");
				break;
			}

			System.out.println("You are currently in Refrigerator " + opsState.currentFridge.fridgeNumber + "-"
					+ opsState.currentFridge.fridgeName);
			
			do {
				if (!menu3FridgeMenu())
					break;
			} while (true);
			break;
		case 3: // Add Fridge
			// show login menu
			if (opsState.currentAccount.acctType.equals("H")) {
				System.out.println("Inspectors cannot add refrigerators.");
			} else {
				System.out.println("Enter refrigerator number: ");
				fridgeNumber = scanner.nextInt();
				scanner.nextLine();

				System.out.println("Enter refrigerator name: ");
				String fridgeName = scanner.nextLine();

				Fridge newFridge = opsState.addFridge(opsState.currentAccount.acctName, fridgeNumber, fridgeName);
				if (newFridge == null) {
					System.out.println("Fridge number already exists");
				} else {
					System.out.println("Refrigerator added: " + newFridge.toString());
				}

			}

			break;

		case 4: // Remove Fridge
			// show list of fridges
			// prompt to enter frig to remove
			// remove fridge
			
			if (opsState.currentAccount.acctType.equals("H")) {
				System.out.println("Inspectors cannot remove refrigerators.");
			} else {
				fridgeList = opsState.findAllFridges(opsState.currentAccount.acctName, opsState.currentAccount.acctType);
				System.out.println("Total refrigerators: " + fridgeList.size());
				for (Fridge item : fridgeList) {
					System.out.println(item.fridgeNumber + "-" + item.fridgeName);
				}
	
				System.out.println("Enter refrigerator number to remove: ");
				fridgeNumber = scanner.nextInt();
				scanner.nextLine();
	
				for (Fridge item : fridgeList) {
					if (item.fridgeNumber == fridgeNumber) {
						opsState.currentFridge = item;
					}
				}
	
				if (opsState.currentFridge == null) {
					System.out.println("Fridge not found.");
					break;
				}
	
				String result = opsState.removeFridge(opsState.currentAccount.acctName, opsState.currentFridge.fridgeNumber,
						opsState.currentFridge.fridgeName);
				System.out.println(result);
				opsState.currentFridge = null;
			}
			
			break;
		case 0: // exit to main menu
			active = false;
			break;
		}
		return active;
	}

	public static boolean menu1main() {

		boolean active = true;

		scanner = new Scanner(System.in);
		System.out.println("Select menu option:");
		System.out.println("");
		String menu1 = "1. Register,2. Login,3. Exit";

		String[] menuList = menu1.split(",");

		for (String item : menuList) {
			System.out.println(item);
		}

		int menuOption = scanner.nextInt();
		scanner.nextLine();

		switch (menuOption) {

		case 1:
			registration();
			break;

		case 2:
			if (login()) {
				// show next menu
				do {
					if (!menu2Account())
						break;
				} while (true);
			}

			break;

		case 3:
			System.out.println("User has been successfully logged out.");
			active = false;
			break;
		}
		return active;

	}

	public static void main(String[] args) {

		// comment this out for production
		createTestAccount();

		do {
			if (!menu1main())
				break;
		} while (true);

		scanner.close();

	}

	public static void createTestAccount() {
//		opsState.createAcct("R", "Test Restaurant", "123", "123");
//		opsState.createAcct("H", "Test Inspector", "456", "456");
//		opsState.addFridge("Test Restaurant", 123, "qwerty");
//		opsState.addFridge("Test Restaurant", 456, "mine");
//		opsState.addFood("Test Restaurant", 123, "rice");
//		opsState.addFood("Test Restaurant", 123, "egg");
//		opsState.addFood("Test Restaurant", 123, "apple");
//		opsState.addFood("Test Restaurant", 456, "bread");
	}
}
