package package1;

import java.util.ArrayList;
import java.util.List;

public class Account {

	public String userName = "";
	public String password = "";
	public String acctName = "";
	public String acctType = "";
	public List<Fridge> fridgeList = new ArrayList<>();
	
	public Account(String acctName, String acctType) {
		this.acctName = acctName;
		this.acctType = acctType;
	}

	public Account(String userName, String password, String acctType) {
		// creating new account
		this.userName = userName;
		this.password = password;
		this.acctType = acctType;
	}

	public Account(String acctName, String userName, String password, String acctType) {
		// creating new account
		this.acctName = acctName;
		this.userName = userName;
		this.password = password;
		this.acctType = acctType;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public void changePassword(String password) {
		this.password = password;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	@Override
	public String toString() {
		return "Accounts [acctName =" + acctName + ", userName=" + userName + "]";
	}

}
