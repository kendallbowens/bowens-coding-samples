package package1;

import java.util.ArrayList;
import java.util.List;

public class Fridge {

	public String owner = ""; //restaurant
	public int fridgeNumber = -1;
	public String fridgeName = "" ;
	public int maxItemsInFridge = 3;
	public List<String> foodList = new ArrayList<>();
	public List<HealthInspector> healthList = new ArrayList<>();
	
	public Fridge(String owner, int fridgeNumber, String fridgeName) {
		this.owner = owner;
		this.fridgeNumber = fridgeNumber;
		this.fridgeName = fridgeName;
		
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String restaurant) {
		this.owner = restaurant;
	}

	public int getFridgeNumber() {
		return fridgeNumber;
	}

	public void setFridgeNumber(int fridgeNumber) {
		this.fridgeNumber = fridgeNumber;
	}

	@Override
	public String toString() {
		return "Fridge [owner=" + owner + ", fridgeNumber=" + fridgeNumber + ", fridgeName=" + fridgeName + "]";
	}

	
	
	

}
