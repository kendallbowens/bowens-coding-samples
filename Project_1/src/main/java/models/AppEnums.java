package models;

import java.util.HashMap;
import java.util.Map;

public class AppEnums {
	
	public enum UserRole {
		Employee(1),
		FinancialManager(2);
		
		private int value;
		private static Map map = new HashMap<>();
		
		private UserRole(int value) {
			this.value = value;
		}
		
		
		static {
			for (UserRole userRole : UserRole.values()) {
				map.put(userRole.value, userRole);
			}
		}
		
		public static UserRole valueOf(int userRole) {
			return (UserRole) map.get(userRole);
		}
		
		public int getValue() {
			return value;
		}
	}
	
	public enum ReimbType{
		Food(1),
		Travel(2),
		Lodging(3),
		Other(4);
		
		private int value;
		private static Map map = new HashMap<>();
		
		private ReimbType (int value) {
			this.value = value;
		}
		
		
		static {
			for (ReimbType userRole : ReimbType.values()) {
				map.put(userRole.value, userRole);
			}
		}
		
		public static ReimbType valueOf(int reimbType) {
			return (ReimbType) map.get(reimbType);
		}
		
		public int getValue() {
			return value;
		}
	}
	
	public enum ReimbStatus{
		Approved(1),
		Denied(2),
		Pending(3);
		
		private int value;
		private static Map map = new HashMap<>();
		
		private ReimbStatus (int value) {
			this.value = value;
		}
		
		
		static {
			for (ReimbStatus userRole : ReimbStatus.values()) {
				map.put(userRole.value, userRole);
			}
		}
		
		public static ReimbStatus valueOf(int reimbStatus) {
			return (ReimbStatus) map.get(reimbStatus);
		}
		
		public int getValue() {
			return value;
		}
		
	}
	

}
