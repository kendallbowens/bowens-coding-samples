package models;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;

import models.AppEnums.ReimbStatus;
import models.AppEnums.ReimbType;

public class Reimbursement {
	
	private int reimbId;
	private int reimbAmt;
	private Timestamp timeSubmitted; 
	private Timestamp timeResolved;
	private String reimbDesc;
	private byte[] receipt;
	private int reimbAuthor;
	private String reimbAuthorName;
	private int reimbResolver;
	private String reimbResolverName;
	private ReimbStatus statusId;
	private ReimbType typeId;

	public Reimbursement() {
		
	}
	
	public Reimbursement(
				  int reimbId
				, int reimbAmt
				, Timestamp timeSubmitted
				, Timestamp timeResolved
				, String reimbDesc
				, byte[] receipt
				, int reimbAuthor
				, String reimbAuthorName
				, int reimbResolver
				, String reimbResolverName
				, ReimbStatus statusId
				, ReimbType typeId) {
		super();
		this.reimbId = reimbId;
		this.reimbAmt = reimbAmt;
		this.timeSubmitted = timeSubmitted;
		this.timeResolved = timeResolved;
		this.reimbDesc = reimbDesc;
		this.receipt = receipt;
		this.reimbAuthor = reimbAuthor;
		this.reimbAuthorName = reimbAuthorName;
		this.reimbResolver = reimbResolver;
		this.reimbResolverName = reimbResolverName;
		this.statusId = statusId;
		this.typeId = typeId;
	}





	public int getReimbId() {
		return reimbId;
	}

	public void setReimbId(int reimbId) {
		this.reimbId = reimbId;
	}

	public int getReimbAmt() {
		return reimbAmt;
	}

	public void setReimbAmt(int reimbAmt) {
		this.reimbAmt = reimbAmt;
	}

	public Timestamp getTimeSubmitted() {
		return timeSubmitted;
	}

	public void setTimeSubmitted(Timestamp timeSubmitted) {
		this.timeSubmitted = timeSubmitted;
	}

	public Timestamp getTimeResolved() {
		return timeResolved;
	}

	public void setTimeResolved(Timestamp timeResolved) {
		this.timeResolved = timeResolved;
	}

	public String getReimbDesc() {
		return reimbDesc;
	}

	public void setReimbDesc(String reimbDesc) {
		this.reimbDesc = reimbDesc;
	}

	public byte[] getReceipt() {
		return receipt;
	}

	public void setReceipt(byte[] receipt) {
		this.receipt = receipt;
	}

	public int getReimbAuthor() {
		return reimbAuthor;
	}

	public void setReimbAuthor(int reimbAuthor) {
		this.reimbAuthor = reimbAuthor;
	}

	public int getReimbResolver() {
		return reimbResolver;
	}

	public void setReimbResolver(int reimbResolver) {
		this.reimbResolver = reimbResolver;
	}

	public ReimbStatus getStatusId() {
		return statusId;
	}

	public void setStatusId(ReimbStatus statusId) {
		this.statusId = statusId;
	}

	public ReimbType getTypeId() {
		return typeId;
	}

	public void setTypeId(ReimbType typeId) {
		this.typeId = typeId;
	}
	

	public String getReimbAuthorName() {
		return reimbAuthorName;
	}

	public void setReimbAuthorName(String reimbAuthorName) {
		this.reimbAuthorName = reimbAuthorName;
	}

	public String getReimbResolverName() {
		return reimbResolverName;
	}

	public void setReimbResolverName(String reimbResolverName) {
		this.reimbResolverName = reimbResolverName;
	}

	@Override
	public String toString() {
		return "Reimbursement [reimbId=" + reimbId + ", reimbAmt=" + reimbAmt + ", timeSubmitted=" + timeSubmitted
				+ ", timeResolved=" + timeResolved + ", reimbDesc=" + reimbDesc + ", receipt="
				+ Arrays.toString(receipt) + ", reimbAuthor=" + reimbAuthor + ", reimbResolver=" + reimbResolver
				+ ", statusId=" + statusId + ", typeId=" + typeId + ", reimbAuthorName=" + reimbAuthorName
				+ ", reimbResolverName=" + reimbResolverName + "]";
	}


	
	
	
	
}