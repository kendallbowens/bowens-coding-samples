package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import models.User;
import services.UserService;

public class LoginController {

	public static void changePassowrd(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		//get values from form
		String password = req.getParameter("password");
		
		//get user
		HttpSession session = req.getSession();
//		User userObj = (User)session.getAttribute("currentUser");
		String username = session.getAttribute("changePwdUser").toString();
		
		//call service and return to home	
	
		UserService userService = new UserService();
		
//		User newUser = userService.getUserByUsername(username);
		
//		System.out.println(newUser);
		
		userService.changePassword(username, password);
		System.out.println(username + " " + password);
		String myPath = "/Project_1/index.html";
		resp.sendRedirect(myPath);

	}
	
	public static void getUserInfo(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		User userObj = (User)session.getAttribute("currentUser");
		
		if (userObj == null) {
			String myPath = "/Project_1/index.html";
			//TODO: also send a message
			resp.sendRedirect(myPath);
			return;
		}
		
		PrintWriter printer = resp.getWriter();
		System.out.println(userObj.toString());
		printer.write(new ObjectMapper().writeValueAsString(userObj));
		
	}
	

	public static void login(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {

		String myPath = null;

		if (!req.getMethod().equals("POST")) {
			myPath = "/index.html";
			req.getRequestDispatcher(myPath).forward(req, resp);
			return;
		}
		

		String username = req.getParameter("username");
		String password = req.getParameter("password");

		UserService userService = new UserService();

		int userId = userService.login(username, password);
		System.out.println(userId + username + password);
		
		if(userId == -1) {
			myPath = "/Project_1/index.html";
			//TODO: also send a message
			resp.sendRedirect(myPath);
			System.out.println("logging in ");

			return;
		}
		else {
			User newUser = userService.getUser(userId);
			
			HttpSession session = req.getSession();
			
			session.setAttribute("currentUser", newUser);
			session.setAttribute("loggedInUsername", newUser.getUsername());
			
			myPath = "/forwarding/home";
			req.getRequestDispatcher(myPath).forward(req, resp);
			return;
		}

	}
}
