package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import models.AppEnums.ReimbStatus;
import models.AppEnums.ReimbType;
import models.AppEnums.UserRole;
import models.Reimbursement;
import models.User;
import services.ReimbursementService;

public class ReimbursementController {

	public static void updateReimb(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, NumberFormatException {
	
		//get user 
		HttpSession session = req.getSession();
		User userObj = (User)session.getAttribute("currentUser");
		
		if (userObj == null) {
			String myPath = "/Project_1/index.html";
			//TODO: also send a message
			resp.sendRedirect(myPath);
			return;
		}
		//TODO: deny access if not Fin Manager
		
		//get values from form
		int reimbId = Integer.parseInt(req.getParameter("reimbId"));
		String reimbDesc = req.getParameter("description");
		System.out.println(ReimbType.valueOf(2));
		ReimbType reimbType = ReimbType.valueOf((req.getParameter("reimbType")));
		ReimbStatus reimbStatus = ReimbStatus.valueOf(Integer.parseInt(req.getParameter("reimbStatus")));
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		int userId = userObj.getUserId();
		System.out.println(userId);
		//call service and then return to home
		
		boolean updateReimb = new ReimbursementService()
				.updateReimbStatus(reimbId, reimbStatus, timestamp, userId);
		System.out.println(updateReimb);
		
		String myPath = "";
		
		if (userObj.getUserRole() == UserRole.FinancialManager) {
			myPath = "/resources/html/new-home-finmanager.html";
		}
		else {
			myPath = "/resources/html/new-home-employee.html";
		}
		req.getRequestDispatcher(myPath).forward(req, resp);
		return;
		
	}
	
	
	
	
	public static void submitReimb(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, NumberFormatException {

		HttpSession session = req.getSession();
		User userObj = (User)session.getAttribute("currentUser");
		
		if (userObj == null) {
			String myPath = "/Project_1/index.html";
			//TODO: also send a message
			resp.sendRedirect(myPath);
			return;
		}
		
		if (!req.getMethod().equals("POST")) {
			String myPath = "/reimb-submission.html";
			req.getRequestDispatcher(myPath).forward(req, resp);
			return;
		}
		
		int reimbTotal = Integer.parseInt(req.getParameter("amount"));
		String reimbDesc = req.getParameter("description");
		ReimbType reimbType = ReimbType.valueOf(Integer.parseInt(req.getParameter("reimbType")));
		ReimbStatus reimbStatus = ReimbStatus.valueOf(Integer.parseInt(req.getParameter("reimbStatus")));
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		//Timestamp timestamp = new Timestamp();
		
		int userId = userObj.getUserId();

		Reimbursement newReimb = new Reimbursement(
				  0
				, reimbTotal
				, timestamp
				, null
				, reimbDesc 
				, null
				, userId
				, null
				, 0
				, null
				, reimbStatus
				, reimbType);
		
		System.out.println(timestamp);

		System.out.println(newReimb.toString());
		ReimbursementService reimbService = new ReimbursementService();
		
		//return new reimbursement as JSON
		Reimbursement reimb = reimbService.sumbitReimbRequest2(newReimb);
		
		if(reimb.getReimbId()== -1) {
			System.out.println("Did not create reimbursement.");
		}
		
		String myPath = "";
		
		if (userObj.getUserRole() == UserRole.FinancialManager) {
			myPath = "/resources/html/new-home-finmanager.html";
		}
		else {
			myPath = "/resources/html/new-home-employee.html";
		}

		req.getRequestDispatcher(myPath).forward(req, resp);
	}

	public static void viewReimbByUser(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {

//		if (!req.getMethod().equals("GET")) {
//			String myPath = "/view-all-reimb.html";
//			req.getRequestDispatcher(myPath).forward(req, resp);
//			return;
//		}
		
		HttpSession session = req.getSession();		
		User userObj = (User)session.getAttribute("currentUser");
		
		if (userObj == null) {
			String myPath = "/Project_1/index.html";
			//TODO: also send a message
			resp.sendRedirect(myPath);
			return;
		}
		
		ReimbStatus filterReimbStatus = (req.getParameter("filterReimbStatus") != null) 
				? ReimbStatus.valueOf(Integer.parseInt(req.getParameter("filterReimbStatus")))
				: null ;
		
		
		
		int userId = userObj.getUserId();
		
		List<Reimbursement> allUserReimbs = new ReimbursementService().getAllReimb(
				userId, userObj.getUserRole(), false);
		
		if(filterReimbStatus == null) {
			System.out.println("no filter");
			allUserReimbs = new ReimbursementService().getAllReimb(userId, userObj.getUserRole(), false);
		}
		else {
			System.out.println("using filter");
			allUserReimbs = new ReimbursementService().getAllReimb(filterReimbStatus);
		}
		
		PrintWriter printer = resp.getWriter();
		System.out.println(allUserReimbs.toString());
		printer.write(new ObjectMapper().writeValueAsString(allUserReimbs));


	}
	
	public static void viewAllReimb(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {

//		if (!req.getMethod().equals("GET")) {
//			String myPath = "/view-all-reimb.html";
//			req.getRequestDispatcher(myPath).forward(req, resp);
//			return;
//		}
		
		//TODO: deny access if not Fin Manager
		
		ReimbStatus filterReimbStatus = (req.getParameter("filterReimbStatus") != null) 
				? ReimbStatus.valueOf(Integer.parseInt(req.getParameter("filterReimbStatus")))
				: null ;
		
		System.out.println("status:" + filterReimbStatus);
		
		List<Reimbursement> allReimbs = new ArrayList<>();
		
		if(filterReimbStatus == null) {
			System.out.println("no filter");
			allReimbs = new ReimbursementService().getAllReimb(0, null, true);
		}
		else {
			System.out.println("using filter");
			allReimbs = new ReimbursementService().getAllReimb(filterReimbStatus);
		}
		
		PrintWriter printer = resp.getWriter();
		System.out.println(allReimbs.toString());
		printer.write(new ObjectMapper().writeValueAsString(allReimbs));
		
		return;


	}
	
	public static void getReimbById(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, JsonProcessingException {
		
//		HttpSession session = req.getSession();
//		
//		Reimbursement reimbObj = (Reimbursement)session.getAttribute("currenReimb");
		
		int reimbId =Integer.parseInt(req.getParameter("reimbId"));
		
		System.out.println(reimbId);
		
		Reimbursement singleReimb = new ReimbursementService().getReimb(reimbId);
		
		PrintWriter printer = resp.getWriter();
		System.out.println(singleReimb.toString());
		printer.write(new ObjectMapper().writeValueAsString(singleReimb));
		
		
		
		
	}
	

}
