package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.AppEnums.UserRole;
import models.User;
import services.UserService;

public class RegistrationController {
	
	public static void usernameExists (HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException, NumberFormatException {
		
		String username = req.getParameter("username");
		System.out.println(username);
		
		
		boolean userExists = new UserService().checkUsernameExists(username);
		System.out.println(userExists);
		
		PrintWriter printer = resp.getWriter();
		printer.write(new ObjectMapper().writeValueAsString(userExists));
		
		//create new session variable
		HttpSession session = req.getSession();
		session.setAttribute("changePwdUser", username);
		
		String myPath = "/Project_1/resources/html/change-pass.html";
		resp.sendRedirect(myPath);
		return;
		
		
		
		
	}
	
	public static void registration (HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException, NumberFormatException {
		
		String myPath = null;
		
		if (!req.getMethod().equals("POST")) {
			myPath = "/resources/html/regis-page.html";
			req.getRequestDispatcher(myPath).forward(req, resp);
			return;
		}
		
		System.out.println("Start creating user");
		
		String username = req.getParameter("username");
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		String passwordRpt = req.getParameter("password-repeat");
		UserRole userRole = (req.getParameter("roleType").equals("1")) ? UserRole.Employee : UserRole.FinancialManager;
		
		User newUser = new User(0, username, password, firstName, lastName, email, userRole);

		System.out.println(newUser.toString());
		
		UserService userService = new UserService();

		int userId = userService.createUser(newUser);
		
		
		myPath = "/index.html";
		req.getRequestDispatcher(myPath).forward(req, resp);
		return;
		
	}

}
