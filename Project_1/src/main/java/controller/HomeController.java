package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.AppEnums.UserRole;
import models.User;

public class HomeController {
	
	public static void home(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		
		User currentUser = (User)session.getAttribute("currentUser");
				
//		if (session.getAttribute("currentUser") == null) {
//			String myPath = "/index.html";
//			req.getRequestDispatcher(myPath).forward(req, resp);
//			return;
//		}	
		if (currentUser == null) {
			String myPath = "/Project_1/index.html";
			//TODO: also send a message
			resp.sendRedirect(myPath);
			return;
		}
		
		String myPath = "";
		
		if (currentUser.getUserRole() == UserRole.FinancialManager) {
			myPath = "/resources/html/new-home-finmanager.html";
		}
		else {
			myPath = "/resources/html/new-home-employee.html";
		}
			
		req.getRequestDispatcher(myPath).forward(req, resp);
		return;
		
	}
	 


}
