package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import models.AppEnums.ReimbStatus;
import models.AppEnums.ReimbType;
import models.AppEnums.UserRole;
import models.Reimbursement;

public class ReimbursementDao implements ReimbursementDaoInterface {

	CustomConnectionFactory myConn = new CustomConnectionFactory();

	public Reimbursement getReimb(int reimbId) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "SELECT * FROM vw_all_remibursements WHERE reimb_id = ?; ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimbId);

			ResultSet rs = ps.executeQuery();

			Reimbursement reimb = new Reimbursement();

			if (rs.next()) {
				reimb = new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount"),
						rs.getTimestamp("reimb_submitted"), rs.getTimestamp("reimb_resolved"), rs.getString("reimb_description"),
						rs.getBytes("reimb_receipt"), rs.getInt("reimb_author"), rs.getString("reimb_author_name")
						, rs.getInt("reimb_resolver"), rs.getString("reimb_resolver_name"),
						ReimbStatus.valueOf(rs.getInt("reimb_status_id")),
						ReimbType.valueOf(rs.getInt("reimb_type_id")));
				return reimb;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public Reimbursement sumbitReimbRequest(double reimbAmt, Timestamp submitTime, String reimbDesc, byte[] receipt,
			int userId, ReimbType type, ReimbStatus status) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "INSERT INTO ers_reimbursement ("
					+ "reimb_amount, " 
					+ "reimb_submitted, "
					+ "reimb_description, "
					+ "reimb_author, "
					+ "reimb_receipt, "
					+ "reimb_type_id," 
					+ "reimb_status_id) " 
					+ "VALUES (?, ?, ?, ?, ?, ?, ?); ";

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, reimbAmt);
			ps.setTimestamp(2, submitTime);
			ps.setString(3, reimbDesc);
			ps.setInt(4, userId);
			ps.setBytes(5, receipt);
			ps.setInt(6, type.getValue());
			ps.setInt(7, status.getValue());

			ps.executeUpdate(); // <--update not query

			Reimbursement reimb = new Reimbursement();
			int insertedPK = -1; // this is a holder variable
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				insertedPK = rs.getInt("reimb_id");
				reimb = this.getReimb(insertedPK);
			}

			myConn.loggy.info("A new reimbursement request was created.");
			return reimb;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown.");
			e.printStackTrace();
			return null;
		}

	}
	
	

	@Override
	public boolean removeReimbursement(int reimbId) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "DELETE FROM ers_reimbursement " + "WHERE reimb_id = ?; ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimbId);

			ps.executeUpdate(); // <--update not query
			myConn.loggy.info("Reimbursement was removed.");
			return true;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Reimbursement updateReimbursement(Reimbursement updateReimb) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "UPDATE ers_reimbursement " + "SET reimb_amount = ?, reimb_submitted = ?, "
					+ "reimb_resolved = ?, reimb_description = ?, "
					+ "reimb_receipt = ?, reimb_author = ?, reimb_resolver = ?, "
					+ "reimb_status_id =?, reimb_type_id = ?" + "WHERE reimb_id = ?; ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setDouble(1, updateReimb.getReimbAmt());
			ps.setTimestamp(2, updateReimb.getTimeSubmitted());
			ps.setTimestamp(3, updateReimb.getTimeResolved());
			ps.setString(4, updateReimb.getReimbDesc());
			ps.setBytes(5, updateReimb.getReceipt());
			ps.setInt(6, updateReimb.getReimbAuthor());
			ps.setInt(7, updateReimb.getReimbResolver());
			ps.setInt(8, updateReimb.getStatusId().getValue());
			ps.setInt(9, updateReimb.getTypeId().getValue());
			ps.setInt(10, updateReimb.getReimbId());

			ps.executeUpdate(); // <--update not query

			myConn.loggy.info("A reimbursement was updated.");

			return this.getReimb(updateReimb.getReimbId());

		} catch (SQLException e) {
			e.printStackTrace();
			myConn.loggy.error("A SQL Exception was thrown.");
			return null;
		}
	}

	@Override
	public List<Reimbursement> getAllReimb(int userId, UserRole role, boolean returnAll) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			// to do:
			// - modify query to retrieve author and resolver names
			// - add as fields to models (???)
			String sql = "SELECT * FROM vw_all_remibursements ";

			if (!returnAll && role == UserRole.Employee) {
				sql = sql + "WHERE reimb_author = ? ";
			} else if (!returnAll && role == UserRole.FinancialManager) {
				sql = sql + "WHERE reimb_resolver = ? ";
			}
			
			sql = sql + " ORDER BY reimb_id; ";

			PreparedStatement ps = conn.prepareStatement(sql);
			
			if (!returnAll) ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();

			List<Reimbursement> listOfReimbs = new ArrayList<>();

			// loop thru & add reimbursement to list
			while (rs.next()) {
				Reimbursement reimb = new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount")
						, rs.getTimestamp("reimb_submitted"), rs.getTimestamp("reimb_resolved"), rs.getString("reimb_description")
						, rs.getBytes("reimb_receipt"), rs.getInt("reimb_author"), rs.getString("reimb_author_name")
						, rs.getInt("reimb_resolver"), rs.getString("reimb_resolver_name")
						, ReimbStatus.valueOf(rs.getInt("reimb_status_id"))
						, ReimbType.valueOf(rs.getInt("reimb_type_id")));

				listOfReimbs.add(reimb);

			}
			myConn.loggy.info("All employee reimbursements are being viewed.");
			return listOfReimbs;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown.");
			e.printStackTrace();
		}

		return null;
	}

	public Reimbursement updateReimbByAuthor(Reimbursement updateReimb) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {
		String sql = "UPDATE ers_reimbursement "
				+ "SET reimb_submitted = ? "
				+ "reimb_description = ?, "
				+ "reimb_receipt = ?"
				+ "WHERE reimb_id = ?; ";
		
		PreparedStatement ps = conn.prepareStatement(sql);	

		ps.setTimestamp(1, updateReimb.getTimeSubmitted());
		ps.setString(2, updateReimb.getReimbDesc());
		ps.setBytes(3, updateReimb.getReceipt());
		ps.setInt(4, updateReimb.getReimbId());
		
		ps.executeUpdate(); // <--update not query

		myConn.loggy.info("A user reimbursement was updated.");

		return this.getReimb(updateReimb.getReimbId());
		
		
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	return null;
	}
	
	public boolean updateReimbStatus(int reimbId, ReimbStatus status, Timestamp statusDate, int statusUser) {
		try (Connection conn = CustomConnectionFactory.getConnection()){
			
			String sql = "UPDATE ers_reimbursement "
					+ "SET reimb_status_id = ? "
					+ ", reimb_resolved = ? "
					+ ", reimb_resolver = ? "
					+ "WHERE reimb_id = ? ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, status.getValue());
			ps.setTimestamp(2, statusDate);
			ps.setInt(3, statusUser);
			ps.setInt(4, reimbId);
			
			System.out.println(ps.toString());
			ps.executeUpdate();
			
			myConn.loggy.info("The status of a reimbursement has been updated.");
			return true;
		}
		catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown.");
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Reimbursement sumbitReimbRequest2(Reimbursement newReimb) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "INSERT INTO ers_reimbursement "
					+ "(reimb_amount, " 
					+ "reimb_submitted, "
					+ "reimb_description, "
					+ "reimb_author, "
					+ "reimb_receipt, "
					+ "reimb_type_id, "
					+ "reimb_status_id) " 
					+ "VALUES (?, ?, ?, ?, ?, ?, ?); ";

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, newReimb.getReimbAmt());
			ps.setTimestamp(2, newReimb.getTimeSubmitted());
			ps.setString(3, newReimb.getReimbDesc());
			ps.setInt(4, newReimb.getReimbAuthor());
			ps.setBytes(5, newReimb.getReceipt());
			ps.setInt(6, newReimb.getTypeId().getValue());
			ps.setInt(7, newReimb.getStatusId().getValue());

			ps.executeUpdate(); // <--update not query

			int insertedPK = -1; // this is a holder variable
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				insertedPK = rs.getInt("reimb_id");
				newReimb.setReimbId(insertedPK) ;
			}

			myConn.loggy.info("A new reimbursement request was created.");
			return newReimb;

		} catch (SQLException e) {
			e.printStackTrace();
			myConn.loggy.error("A SQL Exception was thrown.");
			newReimb.setReimbId(-1);
			return newReimb;
		} catch (NullPointerException n) {
			n.printStackTrace();
			newReimb.setReimbId(-1);
			return newReimb;
		}
	}

	@Override
	public List<Reimbursement> getAllReimb(ReimbStatus status) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			// to do:
			// - modify query to retrieve author and resolver names
			// - add as fields to models (???)
			String sql = "SELECT * FROM vw_all_remibursements ";

			if (status != null) {
				sql = sql + "WHERE reimb_status_id = ? ";
			}

			sql = sql + " ORDER BY reimb_id; ";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			if (status != null) ps.setInt(1, status.getValue());

			ResultSet rs = ps.executeQuery();

			List<Reimbursement> listOfReimbs = new ArrayList<>();

			// loop thru & add reimbursement to list
			while (rs.next()) {
				Reimbursement reimb = new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount")
						, rs.getTimestamp("reimb_submitted"), rs.getTimestamp("reimb_resolved"), rs.getString("reimb_description")
						, rs.getBytes("reimb_receipt"), rs.getInt("reimb_author"), rs.getString("reimb_author_name")
						, rs.getInt("reimb_resolver"), rs.getString("reimb_resolver_name")
						, ReimbStatus.valueOf(rs.getInt("reimb_status_id"))
						, ReimbType.valueOf(rs.getInt("reimb_type_id")));

				listOfReimbs.add(reimb);
			}
			myConn.loggy.info("All employee reimbursement are being viewed.");
			return listOfReimbs;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown.");
			e.printStackTrace();
		}

		return null;
	}
	
	
}
