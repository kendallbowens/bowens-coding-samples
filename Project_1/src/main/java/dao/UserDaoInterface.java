package dao;

import models.User;

public interface UserDaoInterface {

	public int createUser(User newUser);

	public int login(String userName, String password);

	public User getUser(int userId);
	
	public User getUserByUsername(String username);

	public User updateUser(User updateUser);

	public boolean removeUser(int userId);

	public boolean changePassword(String username, String password);
	
	public boolean checkUsernameExists (String username);

}
