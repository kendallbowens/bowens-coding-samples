package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.AppEnums.UserRole;
import models.User;

public class UserDao implements UserDaoInterface {

	CustomConnectionFactory myConn = new CustomConnectionFactory();

	public int createUser(User newUser) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "INSERT INTO ers_users (ers_username, ers_password, " + "user_first_name, user_last_name, "
					+ "user_email, user_role_id) " + "VALUES (?, ?, ?, ?, ?, ?); ";

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, newUser.getUsername());
			ps.setString(2, newUser.getPassword());
			ps.setString(3, newUser.getFirstName());
			ps.setString(4, newUser.getLastName());
			ps.setString(5, newUser.getEmail());
			ps.setInt(6, newUser.getUserRole().getValue());

			ps.executeUpdate(); // <--update not query

			int insertedPK = -1; // this is a holder variable
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				insertedPK = rs.getInt("ers_users_id");
			}

			myConn.loggy.info("A new user was created.");
			return insertedPK;

		} catch (SQLException e) {
			e.printStackTrace();
			myConn.loggy.error("A SQL Exception was thrown.");
			return -1;
		}

	}

	public int login(String username, String password) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "SELECT ers_users_id " + "FROM ers_users " + "WHERE ers_username = ? AND ers_password = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery(); // <--query not update

			if (rs.next()) {
				CustomConnectionFactory.loggy.info("User has been logged in.");
				return rs.getInt("ers_users_id");
			} else {
				return -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			CustomConnectionFactory.loggy.error("A SQL Exception was thrown");
			return -1;
		}

	}

	@Override
	public User getUser(int userId) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "SELECT * FROM ers_users " + "WHERE ers_users_id = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				User newUser = new User(rs.getInt("ers_users_id"), rs.getString("ers_username"),
						rs.getString("user_first_name"), rs.getString("user_last_name"), rs.getString("user_email"),
						UserRole.valueOf(rs.getInt("user_role_id")));

				return newUser;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public User updateUser(User updateUser) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "UPDATE ers_users " + "SET ers_username = ?, user_first_name = ? "
					+ "user_last_name = ?, user_email = ? " + "user_role_id = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, updateUser.getUsername());
			ps.setString(2, updateUser.getFirstName());
			ps.setString(3, updateUser.getLastName());
			ps.setString(4, updateUser.getEmail());
			ps.setInt(5, updateUser.getUserRole().getValue());

			ps.executeUpdate(); // <--update not query

			myConn.loggy.info("A user was updated.");

			return this.getUser(updateUser.getUserId());

		} catch (SQLException e) {
			e.printStackTrace();
			myConn.loggy.error("A SQL Exception was thrown.");
			return null;
		}

	}

	@Override
	public boolean removeUser(int userId) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "DELETE FROM ers_users WHERE ers_users_id = ?; ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, userId);

			ps.executeUpdate(); // <--update not query
			myConn.loggy.info("User was removed.");
			return true;

		} catch (SQLException e) {
			myConn.loggy.error("A SQL Exception was thrown");
			e.printStackTrace();
		}
		return false;
	}

	public boolean changePassword(String username, String password) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {
			String sql = "UPDATE ers_users " 
						+ "SET ers_password = ? " 
						+ "WHERE ers_username = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, password);
			ps.setString(2, username);

			ps.executeUpdate();

			return true;
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public boolean checkUsernameExists(String username) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "SELECT * FROM ers_users " + "WHERE ers_username = ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public User getUserByUsername(String username) {
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String sql = "SELECT * FROM ers_users " + "WHERE ers_username= ? ";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				User newUser = new User(rs.getInt("ers_users_id"), rs.getString("ers_username"),
						rs.getString("user_first_name"), rs.getString("user_last_name"), rs.getString("user_email"),
						UserRole.valueOf(rs.getInt("user_role_id")));

				return newUser;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
}
