package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.HomeController;
import controller.LoginController;
import controller.RegistrationController;
import controller.ReimbursementController;

public class AccountDispatcher {
			
	public static void myVirtualRouter(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		System.out.println(req.getRequestURI());
		switch(req.getRequestURI()) {
		case "/Project_1/forwarding/login":
			System.out.println("case 1");
			LoginController.login(req, resp);
			break;
		
		case "/Project_1/forwarding/home":
			System.out.println("case 2");
			HomeController.home(req, resp);;
			break;
			
		case "/Project_1/forwarding/registration":
			System.out.println("case 3");
			RegistrationController.registration(req, resp);
			break;
			
			
		case "/Project_1/forwarding/reimbsubmit":
			System.out.println("case 5");
			ReimbursementController.submitReimb(req, resp);
			break;
			
		case "/Project_1/json/viewAllReimbs":
			ReimbursementController.viewAllReimb(req, resp);
			System.out.println("case 6");
			break;
		
		case "/Project_1/json/viewByUser":
			ReimbursementController.viewReimbByUser(req, resp);
			System.out.println("case 7");
			break;
			
		case "/Project_1/json/viewByStatus":
			ReimbursementController.viewAllReimb(req, resp);
			System.out.println("case 8");
			break;
			
		case "/Project_1/json/viewById":
			ReimbursementController.getReimbById(req, resp);
			System.out.println("case 9");
			break;
			
		case "/Project_1/forwarding/updatereimb":
			System.out.println("case 10");
			ReimbursementController.updateReimb(req, resp);
			break;		
			
		case "/Project_1/forwarding/checkusername":
			System.out.println("case 11");
			RegistrationController.usernameExists(req, resp);
			break;
			
		case "/Project_1/forwarding/getUserInfo":
			System.out.println("case 12");
			LoginController.getUserInfo(req, resp);
			break;
			
		case "/Project_1/forwarding/passChange":
			System.out.println("case 14");
			LoginController.changePassowrd(req, resp);
			break;
			
		default:
			System.out.println("Bad URI.");
			break;

		}
	}

}
