package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name="MasterServlet", urlPatterns= {"/master/*", "/forwarding/*", "/json/*"})
public class MasterServlet extends HttpServlet {

	
	/*
	 * THE PURPOSE OF THE FRONT CONTROLLER WILL BE TO IMPLEMENT
	 * ANY AUTHENTIFICATION LOGIC YOU MAY HAVE.
	 * 
	 * It modularizes your security.
	 */
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		System.out.println("MASTER SERVLET: In the doGet method!!!");
		
		AccountDispatcher.myVirtualRouter(req, resp); //HERE I am offloading my work to another entity
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		System.out.println("MASTER SERVLET: In the doPost method!!!");
		
		doGet(req, resp);
	}
}

