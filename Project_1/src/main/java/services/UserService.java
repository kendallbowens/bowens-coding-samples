package services;

import dao.UserDao;
import dao.UserDaoInterface;
import models.User;

public class UserService implements UserServiceInterface {

	UserDaoInterface myDao = new UserDao();

	@Override
	public int createUser(User newUser) {
		return myDao.createUser(newUser);
	}

	@Override
	public int login(String userName, String password) {
		return myDao.login(userName, password);
	}

	@Override
	public User getUser(int userId) {
		return myDao.getUser(userId);
	}

	@Override
	public User updateUser(User updateUser) {
		return myDao.updateUser(updateUser);
	}

	@Override
	public boolean removeUser(int userId) {
		return myDao.removeUser(userId);
	}
	
	@Override
	public boolean changePassword(String username, String password) {
		return myDao.changePassword(username, password);
	}

	@Override
	public boolean checkUsernameExists(String username) {
		return myDao.checkUsernameExists(username);
	}

	@Override
	public User getUserByUsername(String username) {
		return myDao.getUserByUsername(username);
	}

}
