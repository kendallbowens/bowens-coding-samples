package services;

import java.sql.Timestamp;
import java.util.List;

import dao.ReimbursementDao;
import dao.ReimbursementDaoInterface;
import models.AppEnums.ReimbStatus;
import models.AppEnums.ReimbType;
import models.AppEnums.UserRole;
import models.Reimbursement;

public class ReimbursementService implements ReimbursementServiceInterface {
	
	ReimbursementDaoInterface myDao = new ReimbursementDao();

	@Override
	public Reimbursement sumbitReimbRequest(double reimbAmt, Timestamp submitTime, String reimbDesc,
			byte[] receipt, int userId, ReimbType type, ReimbStatus status) {
		return myDao.sumbitReimbRequest(reimbAmt, submitTime, reimbDesc, receipt, userId, type, status);
	}
	
	

	@Override
	public Reimbursement getReimb(int reimbId) {		
		return myDao.getReimb(reimbId);
	}

	@Override
	public Reimbursement updateReimbursement(Reimbursement updateReimb) {	
		return myDao.updateReimbursement(updateReimb);
	}

	@Override
	public boolean removeReimbursement(int reimbId) {		
		return myDao.removeReimbursement(reimbId);
	}

	@Override
	public List<Reimbursement> getAllReimb(int userId, UserRole role, boolean returnAll) {
		return myDao.getAllReimb(userId, role, returnAll);
	}

	@Override
	public Reimbursement updateReimbByAuthor(Reimbursement updateReimb) {
		return myDao.updateReimbByAuthor(updateReimb);
	}

	@Override
	public boolean updateReimbStatus(int reimbId, ReimbStatus status, Timestamp statusDate, int statusUse) {
		return myDao.updateReimbStatus(reimbId, status, statusDate, statusUse);
	}



	@Override
	public Reimbursement sumbitReimbRequest2(Reimbursement newReimb) {
		return myDao.sumbitReimbRequest2(newReimb);
	}



	@Override
	public List<Reimbursement> getAllReimb(ReimbStatus status) {
		return myDao.getAllReimb(status);
	}

}
