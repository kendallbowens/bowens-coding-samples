package services;

import java.sql.Timestamp;
import java.util.List;

import models.AppEnums.ReimbStatus;
import models.AppEnums.ReimbType;
import models.AppEnums.UserRole;
import models.Reimbursement;

public interface ReimbursementServiceInterface {

	public Reimbursement sumbitReimbRequest(double reimbAmt, Timestamp submitTime, String reimbDesc, byte[] receipt,
			int userId, ReimbType type, ReimbStatus status);
	
	public Reimbursement sumbitReimbRequest2(Reimbursement newReimb);

	public Reimbursement getReimb(int reimbId);

	public List<Reimbursement> getAllReimb(int userId, UserRole role, boolean returnAll);
	
	public List<Reimbursement> getAllReimb(ReimbStatus status);

	public Reimbursement updateReimbursement(Reimbursement updateReimb);

	public boolean removeReimbursement(int reimbId);

	public Reimbursement updateReimbByAuthor(Reimbursement updateReimb);

	public boolean updateReimbStatus(int reimbId, ReimbStatus status, Timestamp statusDate, int statusUse);

}
