console.log("in user js file");


var getUserInfo = function(){
    let URL = 'http://localhost:8080/Project_1/forwarding/getUserInfo';

    fetch(URL)
            .then(function(response){
                        const convertedResponse = response.json();
                        return convertedResponse;
                    })
            .then(function(secondResponse){
                        console.log(secondResponse);
                
                        var displayName = secondResponse.firstName + ' ' + secondResponse.lastName;
                        document.getElementById('displayName').append(document.createTextNode(displayName));
                        var displayRole = (secondResponse.userRole === 'FinancialManager') ? 'Financial Manager' : secondResponse.userRole;
                        document.getElementById('displayRole').append(document.createTextNode(displayRole));
                        
                    })
}

