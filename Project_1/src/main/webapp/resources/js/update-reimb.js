console.log("in js file");

let URL = 'http://localhost:8080/Project_1/json/viewById';

let reimbId = getQueryStringValue('reimbId'); //replace with passed in id
URL = URL + '?reimbId=' + reimbId;

//found function to parse the query string parameter passed in
function getQueryStringValue (key) {  
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
  }  

window.onload = function(){

	ajaxGetReimbursement();
	//clickerFunction();
	//console.log("create listener");
}

function ajaxGetReimbursement(){

	console.log("starting the ajax request");
	
	fetch(URL)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					reimbDOMManipulation(secondResponse);
				})
}

function reimbDOMManipulation(reimbJSON){

    //set values of the form elements
    document.getElementById('amount').value = reimbJSON.reimbAmt;
    document.getElementById('description').value = reimbJSON.reimbDesc;
    document.getElementById('reimbType').value = reimbJSON.typeId;
    document.getElementById('reimbId').value = reimbId

    switch (reimbJSON.statusId) {
        case "Approved":
            document.getElementById('approvedStatus').checked = true;
            break;
        case "Denied":
            document.getElementById('deniedStatus').checked = true;
            break;
        case "Pending":
            document.getElementById('pendingStatus').checked = true;
            break;
        default:
            break;
    }
        


}