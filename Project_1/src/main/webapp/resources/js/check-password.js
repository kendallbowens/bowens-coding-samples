console.log("in js file");

var check = function() {
    if (document.getElementById('password').value != "" 
    	&& document.getElementById('passwordRpt').value != "" 
    	&& document.getElementById('password').value == document.getElementById('passwordRpt').value) {
      document.getElementById('message').style.color = 'green';
      document.getElementById('message').innerHTML = 'Passwords match';
      document.getElementById('createBtn').disabled = false;
    } else {
      document.getElementById('message').style.color = 'red';
      document.getElementById('message').innerHTML = 'Passwords do not match';
      document.getElementById('createBtn').disabled = true;
    }
  }

var checkUsername = function(){
    let URL = 'http://localhost:8080/Project_1/forwarding/checkusername';

    let username = document.getElementById('username').value;
    URL = URL + '?username=' + username;
    console.log(URL);

    fetch(URL)
            .then(function(response){
                        const convertedResponse = response.json();
                        return convertedResponse;
                    })
            .then(function(secondResponse){
                        console.log(secondResponse);

                        document.getElementById('messageDiv').hidden = false;

                        if(secondResponse == true){
                            document.getElementById('userExistMessage').style.color = 'red';
                            document.getElementById('userExistMessage').innerHTML = 'Username already exists';
                            document.getElementById('createBtn').disabled = true;
                        }
                        else{
                            document.getElementById('userExistMessage').style.color = 'green';
                            document.getElementById('userExistMessage').innerHTML = 'Username is available';
                            document.getElementById('createBtn').disabled = false;
                            check();
                        }
                    })
}
