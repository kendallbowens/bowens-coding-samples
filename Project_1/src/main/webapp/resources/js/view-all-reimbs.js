
console.log("in js file");

let URL = 'http://localhost:8080/Project_1/json/viewAllReimbs'
let formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD'
 });

window.onload = function(){
    console.log(URL);
	ajaxViewAllReimbs();
	getUserInfo();
	//clickerFunction();
	//console.log("create listener");
}

function ajaxViewAllReimbs(){

	console.log("starting the ajax request");
	
	
	fetch(URL)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
					console.log(secondResponse);
					reimbDOMManipulation(secondResponse);
				})
}

function clickerFunction(){
    document.getElementById("filterReimbStatus").addEventListener("onchange", getStatus)
}

function getStatus(){

    let url = 'http://localhost:8080/Project_1/json/viewByStatus?filterReimbStatus='
        + document.getElementById("filterReimbStatus").value;

    console.log(url);
    
    fetch(url)
		.then(function(response){
					const convertedResponse = response.json();
					return convertedResponse;
				})
		.then(function(secondResponse){
                    console.log("clicked");
					console.log(secondResponse);
					deleteTable(secondResponse);
                    console.log("after delete:" + document.querySelector("#reimbTableBody").childElementCount);
                    reimbDOMManipulation(secondResponse);
                    console.log("after repopulate:" + document.querySelector("#reimbTableBody").childElementCount);
				})


}

function deleteTable(statJSON){
	let rowCount = document.querySelector("#reimbTableBody").childElementCount
    console.log(document.querySelector("#reimbTableBody").childElementCount);

    for (let i= 0; i < rowCount ; i++) {
        document.querySelector("#reimbTableBody").deleteRow(0);
	}
}

// function statusTable(statusJSON, status){
//     console.log("inside the status table function");

//     const reimbStatus = [];
//     for(let i = 0; i < statusJSON.length; i++){
//         if (statusJSON.status === status){
//             reimbStatus.push(statusJSON[i])
//         }
//     }
// }


function reimbDOMManipulation(reimbJSON){
	
	for(let i=0; i < reimbJSON.length ; i++){
		
		let newTR = document.createElement("tr");
		let newTH = document.createElement("th");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		let newTD4 = document.createElement("td");
		let newTD5 = document.createElement("td");
		let newTD6 = document.createElement("td");
		let newTD7 = document.createElement("td");
		let newTD8 = document.createElement("td");
		let newTD9 = document.createElement("td");
		let newTD10 = document.createElement("td");
		
		
		newTH.setAttribute("scope", "row");
		let myTextH = document.createTextNode(reimbJSON[i].reimbId);
        let myTextD1 = document.createTextNode(formatter.format(reimbJSON[i].reimbAmt));
        let myTextD2 = document.createTextNode(new Date(reimbJSON[i].timeSubmitted).toLocaleString());
        let myTextD3 = (reimbJSON[i].timeResolved != null ) ? 
        	document.createTextNode(new Date(reimbJSON[i].timeResolved).toLocaleString())
        	: document.createTextNode('');;
        let myTextD4 = document.createTextNode(reimbJSON[i].reimbDesc);
        //let myTextD5= document.createTextNode(reimbJSON[i].reimb_receipt);
        let myTextD6 = document.createTextNode(reimbJSON[i].reimbAuthorName);
        let myTextD7 = document.createTextNode(reimbJSON[i].reimbResolverName);
        let myTextD8 = document.createTextNode(reimbJSON[i].statusId);
        let myTextD9 = document.createTextNode(reimbJSON[i].typeId);

        //create button 
        let newLink = document.createElement('a');
        let linkButton = document.createElement("button");
        linkButton.innerHTML = "Review";
        linkButton.className = 'reviewButton';
        newLink.appendChild(linkButton);
        newLink.title = 'Review';
        newLink.href = 'http://localhost:8080/Project_1/resources/html/update-reimb.html?reimbId=' + reimbJSON[i].reimbId;

        let myItemD10 = newLink;

        newTH.appendChild(myTextH);
        newTD1.appendChild(myTextD1);
        newTD2.appendChild(myTextD2);
        newTD3.appendChild(myTextD3);
        newTD4.appendChild(myTextD4);
        //newTD5.appendChild(myTextD5);
        newTD6.appendChild(myTextD6);
        newTD7.appendChild(myTextD7);
        newTD8.appendChild(myTextD8);
        newTD9.appendChild(myTextD9);
        newTD10.appendChild(myItemD10);
        
		
        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(newTD3);
        newTR.appendChild(newTD4);
        //newTR.appendChild(newTD5);
        newTR.appendChild(newTD6);
        newTR.appendChild(newTD7);
        newTR.appendChild(newTD8);
        newTR.appendChild(newTD9);
        newTR.appendChild(newTD10);

        let newSelection = document.querySelector("#myReimbTable").querySelector("#reimbTableBody");
        newSelection.appendChild(newTR);
		
	}
	
}

